from odoo import models, fields , api , exceptions
import logging

class EmployeeReport(models.TransientModel):
   _name = 'employee.report'
   _description = 'Employee Report'
  
   branch_ids = fields.Many2many(comodel_name='res.branch',string='Branches' )
   department_ids = fields.Many2many(comodel_name='hr.department',string='Department')
   job_position_ids = fields.Many2many(comodel_name='hr.job' , string='Job Position')
   
   def action_print_xlsx_report(self):
      # domain = [('department_id','=',self.env.user.department_id.id)]
      # print("domain",domain)

      # emp_obj =self.env['hr.employee'].search_read([])

      data={
          'model':'employee.report',
           'form':self.read()[0],
         # 'emp_obj':emp_obj,
         # 'form_data':self.read()[0]
      }
      return self.env.ref('hr_employee_report.employee_report_xlsx').report_action(self,data=data)
