import base64
import io
from odoo import models
import datetime

class EmployeeReportXlsx(models.AbstractModel):
    _name = 'report.hr_employee_report.employee_report_xls'
    _inherit = 'report.report_xlsx.abstract'
    
   
    def generate_xlsx_report(self, workbook, data, emp):
        # print("nnnnnnnnnnnnn", data['emp_obj'])

        company = self.env.company
        
        # date format
        def get_date_format(date):
            return date.strftime("%d/%m/%Y")
            
        # Create Designs
        heading = workbook.add_format({
            "bold": 1,
            "top": 2,
            "bottom": 2,
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'white',
            "bg_color": 'DF00FE',
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        seial_number = workbook.add_format({
            "border_color": "#ABABAB",
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'white',
            "bg_color": 'DF00FE',
        })
        worksheet = workbook.add_worksheet('Employee Report')
        bold = workbook.add_format({'bold':True})
        worksheet.set_column('A:Z',15)
        worksheet.set_column('AA2:AT2',20)
     
        row = 1
        col = 0

        # Main heading of comapny data
        worksheet.merge_range('F1:K2', company.name, company_data)
        worksheet.merge_range('F3:K3', company.street ,company_data)
        worksheet.merge_range('F4:K4', company.email, company_data)
        worksheet.merge_range('F5:K5',company.phone ,company_data)


        # Main Heading Employee Data
        worksheet.merge_range('A8:T8', 'Employee Data', heading)

        #sub heading of Employee Data
        # worksheet.write('A7', 'Branches', heading)
        worksheet.write('A9', 'S.No', heading)
        worksheet.write('B9','Employee ID',heading)
        worksheet.write('C9','Name',heading)
        worksheet.write('D9','Department',heading)
        worksheet.write('E9','Job Position',heading)
        worksheet.write('F9','Work Mobile',heading)
        worksheet.write('G9','Work Phone',heading)
        worksheet.write('H9','Work Email',heading)
        worksheet.write('I9','Work Location',heading)
        worksheet.write('J9','Department',heading)
        worksheet.write('K9','Grade ',heading)
        worksheet.write('L9','Responsible To ',heading)
        worksheet.write('M9','Father Name ',heading)
        worksheet.write('N9','Email ',heading)
        worksheet.write('O9','CNIC',heading)
        worksheet.write('P9','Gender ',heading)
        worksheet.write('Q9','Date of Birth ',heading)
        worksheet.write('R9','Religion ',heading)
        worksheet.write('S9','Branch',heading)
        worksheet.write('T9','Date of join ',heading)


        # Main Heading Academic Qualification
        worksheet.merge_range('U8:Z8', 'Academic Qualification', heading)

        # Sub Heading of Academic Qualification
        worksheet.write('U9', 'Degree', heading)
        worksheet.write('V9', 'Degree Program', heading)
        worksheet.write('W9', 'Institute Name', heading)
        worksheet.write('X9', 'Start Date', heading)
        worksheet.write('Y9', 'End Date', heading)
        worksheet.write('Z9', 'CGPA', heading)
       

       # Main Heading Professional Qualification
        worksheet.merge_range('AA8:AG8', 'Professional Qualification', heading)

       # sub heading Professional Qualification
        worksheet.write('AA9', 'Degree', heading)
        worksheet.write('AB9', 'Degree Program', heading)
        worksheet.write('AC9', 'Institute Name', heading)
        worksheet.write('AD9', 'Start Date', heading)
        worksheet.write('AE9', 'End Date', heading)
        worksheet.write('AF9', 'Duration', heading)
        worksheet.write('AG9', 'CGPA', heading)

        # Main Heading Experience
        worksheet.merge_range('AH8:AM8', 'Experience', heading)

        # sub heading Experience
        worksheet.write('AH9', 'Company Name', heading)
        worksheet.write('AI9', 'Job Position', heading)
        worksheet.write('AJ9', 'Start Date', heading)
        worksheet.write('AK9', 'End Date', heading)
        worksheet.write('AL9', 'Duration', heading)
        worksheet.write('AM9', 'Remarks', heading)

         # Main Professional Skills
        worksheet.write('AN8', 'Professional Skills', heading)

        # sub heading Professional Skills
        worksheet.write('AN9', 'Name', heading)


        # Main Heading Employee Dependent
        worksheet.merge_range('AO8:AT8', 'Employee Dependent', heading)

        # sub heading Employee Dependent
        worksheet.write('AO9', 'Dependent Name', heading)
        worksheet.write('AP9', 'Relation', heading)
        worksheet.write('AQ9', 'D.O.B', heading)
        worksheet.write('AR9', 'Age', heading)
        worksheet.write('AS9', 'Gender', heading)
        worksheet.write('AT9', 'CNIC', heading)

        domain = []
        if data['form']['branch_ids']:
            domain.append(('branch_id', 'in', data['form']['branch_ids']))
        if data['form']['department_ids']:
            domain.append(('department_id', 'in', data['form']['department_ids'])) 
        if data['form']['job_position_ids']:
            domain.append(('job_id', 'in', data['form']['job_position_ids']))

        employees = self.env['hr.employee'].search(domain)
        
        row = 8
        s_no =  1

        for emp in employees:
            row += 1
            # worksheet.write(row, col, emp.branch_id.name,center)
            worksheet.write(row, 0,s_no,seial_number)
            worksheet.write(row,col + 1,emp.emp_id,center)
            worksheet.write(row,col + 2,emp.name,center)
            worksheet.write(row,col + 3,emp.department_id.name,center)
            worksheet.write(row,col + 4,emp.job_id.name,center)
            worksheet.write(row,col + 5,emp.mobile_phone,center)
            worksheet.write(row,col + 6,emp.work_phone,center)
            worksheet.write(row,col + 7,emp.work_email,center)
            worksheet.write(row,col + 8,emp.work_location_id.name,center)
            worksheet.write(row,col + 9,emp.department_id.name,center)
            worksheet.write(row,col + 10,emp.grade_id.name,center)
            worksheet.write(row,col + 11,emp.parent_id.name,center)
            worksheet.write(row,col + 12,emp.father,center)
            worksheet.write(row,col + 13,emp.private_email,center)
            worksheet.write(row,col + 14,emp.national_id.nationality_name,center)
            worksheet.write(row,col + 15,emp.gender,center)
            worksheet.write(row,col + 16, get_date_format(emp.birthday),center) 
            worksheet.write(row,col + 17,emp.religion_id.name,center)
            worksheet.write(row,col + 18,emp.branch_id.name,center)
            worksheet.write(row,col + 19, get_date_format(emp.joining_date),center)

            row_q = row
            r1 = row
            r2 = row
            r3 = row

            for qualification in emp.qualification_ids:
                worksheet.write(row,20, qualification.degree_name.name, center)
                worksheet.write(row, 21, qualification.degree_prgrm.name, center)
                worksheet.write(row, 22, qualification.institute_name.name , center)
                worksheet.write(row, 23, get_date_format(qualification.deg_st_date) , center)
                worksheet.write(row, 24, get_date_format(qualification.deg_nd_date ), center)
                worksheet.write(row, 25, qualification.deg_percentage if qualification.deg_percentage  else ' ', center)
                row += 1

            for professional in emp.crfct_ids:
                worksheet.write(row_q,26, professional.crtfct_title.name, center)
                worksheet.write(row_q, 27, professional.crtfct_field, center)
                worksheet.write(row_q, 28, professional.institute_name.name , center)
                worksheet.write(row_q, 29, str(professional.crtfct_st_date) , center)
                worksheet.write(row_q, 30, str(professional.crtfct_nd_date) , center)
                worksheet.write(row_q, 31, professional.total_exp_dur , center)
                worksheet.write(row_q, 32, professional.crtfct_percentage if professional.crtfct_percentage else ' ', center)
                row_q += 1


            for experience in emp.experience_ids:
                worksheet.write(r1, 33, experience.company_name.name, center)
                worksheet.write(r1, 34, experience.exp_job_id , center)
                worksheet.write(r1, 35, get_date_format(experience.exp_st_date) , center)
                worksheet.write(r1, 36, get_date_format(experience.exp_nd_date) , center)
                worksheet.write(r1, 37, experience.total_exp_dur , center)
                worksheet.write(r1, 38, experience.remarks if experience.remarks else ' ', center)
                r1 += 1

            for skill in emp.skill_ids:
                worksheet.write(r2, 39, skill.name if skill.name else ' ', center)
                r2 += 1  

            for dependent in emp.dependent_ids:
                worksheet.write(r3, 40, dependent.name , center)
                worksheet.write(r3, 41, dependent.relation , center)
                worksheet.write(r3, 42, str(dependent.dob) , center)
                worksheet.write(r3, 43, dependent.age , center)
                worksheet.write(r3, 44, dependent.gender , center)
                worksheet.write(r3, 45, dependent.cnic if dependent.cnic else ' ', center)
                r3 += 1       
            
            row  = row_q
            row = r1
            row = r2
            row = r3
            row += 1  
            s_no += 1   

                