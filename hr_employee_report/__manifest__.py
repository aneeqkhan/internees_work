{
'name': "HR Employee Reports",
'summary': "Employee Excel Reports",
'description': """
Employee Reports
==============
Generate Employee Excel Reports.
""",
'author': "romayyah",
'website': "http://www.example.com",
'category': '',
'version': '13.0.1',
'depends': [ 'base','hr','res_branch','report_xlsx','hr_contract','hr_employee_extension'],
'data': [
   'views/employee.xml',
   'report/report.xml',
   'security/ir.model.access.csv'
    ],
}