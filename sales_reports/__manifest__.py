{
    'name': 'Sales Reports ',
    'author': 'romayyah',
    'category': 'sales',
    'description': """Generate sales excel reports""",
    'summary': 'Sales Report',
    'depends': ['sale','report_xlsx','sale_management'],
    'license': 'LGPL-3',
    'data': [
        'security/ir.model.access.csv',
        'views/sale_report.xml',
        'reports/report.xml',     
    ],
}