from odoo import models, fields 
from datetime import datetime, date, timedelta
import logging

class SalesReport(models.TransientModel):
   _name = 'sales.report'
   _description = 'Sales Report'

   partner_ids = fields.Many2many(comodel_name='res.partner' , string='Customer')
   # user_ids = fields.Many2many(comodel_name="res.users" ,string='Sales Person')
   date_orders = fields.Datetime(string ='Order Date')
   
   def action_print_xlsx_report(self):
      data={
         'model':'sales.report',
         'form':self.read()[0],
      }
      return self.env.ref('sales_reports.sales_report_xlsx').report_action(self,data=data)