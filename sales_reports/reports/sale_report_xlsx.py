from datetime import datetime, date, timedelta
from datetime import datetime
import base64
import io
from typing import NoReturn
from odoo import models

class SaleReportXlsx(models.AbstractModel):
    _name = 'report.sales_reports.sales_report_xls'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, sales_report):
        #company	
        company = self.env.company
            
        #Design
        heading2 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#D3D3D3',
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        seial_number = workbook.add_format({
            "border_color": "#ABABAB",
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'black',
            "bg_color": '#D3D3D3',
        })
        worksheet = workbook.add_worksheet('Quotations Report')
        worksheet.set_column('A:G',20)
        worksheet.set_column('H9:H9',30)
        worksheet.set_column('I:X',20)
     
        row = 1
        col = 0

        # Main heading of comapny data
        worksheet.merge_range('D1:G2', company.name, company_data)
        worksheet.merge_range('D3:G3', company.street ,company_data)
        worksheet.merge_range('D4:G4', company.email, company_data)
        worksheet.merge_range('D5:G5',company.phone ,company_data)

        #heading of sales
        worksheet.write('A9','No.',heading2)
        worksheet.write('B9', 'Customer Name', heading2)
        worksheet.write('C9', 'Invoice Address', heading2)
        worksheet.write('D9','Delivery Address',heading2)
        worksheet.write('E9','Order Date',heading2)
        worksheet.write('F9','Payment Terms',heading2)
        worksheet.write('G9','Product',heading2)
        worksheet.write('H9','Description',heading2)
        worksheet.write('I9','Quantity',heading2)
        worksheet.write('J9','Delivered',heading2)
        worksheet.write('K9','Invoiced',heading2)
        worksheet.write('L9','Unit Price', heading2)
        worksheet.write('M9','Taxes',heading2)
        worksheet.write('N9','Sub Total',heading2)
        worksheet.write('O9','Untaxed Amount', heading2)
        worksheet.write('P9','Taxes',heading2)
        worksheet.write('Q9','Total',heading2)
        worksheet.write('R9','Margin',heading2)
        worksheet.write('S9','Sales Person',heading2)
        worksheet.write('T9','Customer Reference',heading2)
        worksheet.write('U9','Shipping Policy',heading2)
        worksheet.write('V9','Delivery Date',heading2)
        worksheet.write('W9','Effective Date',heading2)
        worksheet.write('X9','Sale order status',heading2)

    
        domain = []
        if data['form']['partner_ids']:
            domain.append(('partner_id', 'in', data['form']['partner_ids']))
        # if data['form']['user_ids']:
        #     domain.append(('user_id', 'in', data['form']['user_ids']))
        if data['form']['date_orders']:
            domain.append(('date_order', '=', data['form']['date_orders']))         
     
        sales_report = self.env['sale.order'].search(domain)    

        row = 8
        s_no =  1
        for sales in sales_report:
            row += 1
            worksheet.write(row, 0,s_no,seial_number)
            worksheet.write(row,col + 1,sales.partner_id.name if sales.partner_id.name else ' ' ,center)
            worksheet.write(row,col + 2,sales.partner_invoice_id.name if sales.partner_invoice_id.name else ' ' ,center)
            worksheet.write(row,col + 3,sales.partner_shipping_id.name if sales.partner_shipping_id.name else ' ' ,center)
            worksheet.write(row,col + 4, str(sales.date_order if sales.date_order else ' ' ),center)
            worksheet.write(row,col + 5, sales.payment_term_id.name if sales.payment_term_id.name else ' ' ,center)
            
            #,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            worksheet.write(row,14, sales.amount_untaxed if sales.amount_untaxed else 0 , center) 
            worksheet.write(row,15, sales.amount_tax if sales.amount_tax else 0 , center) 
            worksheet.write(row,16, sales.amount_total if sales.amount_total else 0 , center) 
            worksheet.write(row,17, sales.margin if sales.margin else 0 , center) 
            worksheet.write(row,18, sales.user_id.name if sales.user_id.name else 0 , center)
            worksheet.write(row,19, sales.client_order_ref if sales.client_order_ref else 0 , center)
            worksheet.write(row,20, sales.picking_policy if sales.picking_policy else 0 , center)
            worksheet.write(row,21, str(sales.commitment_date if sales.commitment_date else 0 ) , center)
            worksheet.write(row,22, str(sales.effective_date if sales.effective_date else 0) , center)
            worksheet.write(row,23, sales.state if sales.state else 0 , center)

            #order_line
            for lines in sales.order_line:
                worksheet.write(row,6, lines.product_id.name if lines.product_id.name  else ' ', center)
                worksheet.write(row,7, lines.name if lines.name else ' ', center)
                worksheet.write(row,8, lines.product_uom_qty if lines.product_uom_qty else 0 , center)
                worksheet.write(row,9, lines.qty_delivered if lines.qty_delivered  else 0 , center)
                worksheet.write(row,10, lines.qty_invoiced if lines.qty_invoiced  else 0 , center)
                worksheet.write(row,11, lines.price_unit if lines.price_unit else 0 , center)
                worksheet.write(row,12, lines.tax_id.name if lines.tax_id.name else 0 , center)
                worksheet.write(row,13, lines.price_subtotal if lines.price_subtotal else 0 , center)
                row += 1
        
            # row += 2
            s_no += 1