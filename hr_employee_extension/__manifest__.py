# -*- coding: utf-8 -*-
{
    'name': "HR Employee Extension",
    'summary': """
        Enhanced HR Profile 
    """,
    'author': "Aneeq Ul Haq",
    'version': '0.1',
    'category': 'hr',
    'depends': [
        'base',
        'hr',
        "res_branch",
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'data/sequence.xml',
        'views/hr_employee_view.xml',
        'views/grade_type.xml',
        'views/views.xml',
        'reports/employee_data_form.xml',

    ],

}
