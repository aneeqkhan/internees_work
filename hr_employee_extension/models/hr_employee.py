# -*- coding: utf-8 -*-

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from ast import literal_eval
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError



class Employee(models.Model):
    _inherit = 'hr.employee'

    joining_date = fields.Date("Date of Join")
    religion = fields.Selection([('Muslim', 'Muslim'), ('NonMuslim', 'Non Muslim')], string="Religion")
    passport = fields.Many2one('hr.passport', string="Passport")
    doc_type = fields.One2many('hr.emp.doc', 'hrdoc', string="Doc Type")
    emergency = fields.One2many('hr.emergency.contact', 'emergency_employee', string="Emergency Contacts")
    education = fields.One2many('hr.education', 'education_emp', string="Education")
    national_id = fields.Many2one('hr.nationality', string="National ID")
    work_permit = fields.One2many('work.permit', 'employee', string="Work Permit", groups="hr.group_hr_user")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Approved')], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', track_sequence=3,
        default='draft', groups="hr.group_hr_user")
    total_years = fields.Char("Total Years/Days", compute="_compute_dateofjoin")
    grade_id = fields.Many2one('grade.type', string="Grade",store=True, related="job_id.grade_id")
    experience_ids= fields.One2many('hr.employee.experiance', 'employee_ref', string='Experience', help='Experiance Information')
    skill_ids = fields.One2many('employee.skill', 'employee_id', string='Skills')
    qualification_ids= fields.One2many('hr.employee.qualification', 'employee_ref', string='Qualification', help='Qualification Information')
    crfct_ids= fields.One2many('hr.employee.certificate', 'employee_ref', string='Certification', help='Certification Information')
    father = fields.Char("Father Name")
    domicile = fields.Many2one(comodel_name = 'hr.domicile',string='Domicile')
    
    branch_id = fields.Many2one('res.branch', 'Branch', default=lambda self: self.env.user.default_branch_id)
    comments = fields.Html("Comments")
    emp_seq = fields.Char("Employee Sequence",readonly=True, copy=False, default='/')
    emp_id = fields.Char("Employee ID")
    work_location_id = fields.Many2one(comodel_name = 'hr.employee.work.location',string='Work Location')

    # Bank Information
    banks_account_id = fields.One2many(
        'res.partner.bank', 'res_partner_ref','Bank Account Number',
        groups="hr.group_hr_user",
        help='Employee bank salary account')
    
    branch_code = fields.Char(string='Branch Code')
    acc_title = fields.Char(string='Account Title')
    IBAN = fields.Char(string='IBAN', size=128)
    acc_number = fields.Char('Account Number')
    bank_id = fields.Many2one('res.bank', string='Bank')

    present_address = fields.Char(string='Present Address')
    p_street = fields.Char()
    p_street2 = fields.Char()
    p_zip = fields.Char()
    p_city = fields.Char()
    p_state_id = fields.Many2one('res.country.state',  string="Fed. State",domain="[('country_id', '=?', p_country_id )]",  )
    p_country_id = fields.Many2one('res.country',  string="Country")
    permanent_address = fields.Char(string='Permanent Address')
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char()
    city = fields.Char()
    state_id = fields.Many2one('res.country.state',  string="Fed. State",domain="[('country_id', '=?', country_id)]")
    
    is_check = fields.Boolean("Copy data to permanent address",store=True )
    tehsil = fields.Char("Tehsil")
    religion_id = fields.Many2one(comodel_name='hr.employee.religion', 
        string='Religion',
    )
    analytic_account_id = fields.Many2one(string='Project', comodel_name='account.analytic.account')
    dependent_ids = fields.One2many('hr.dependent', 'employee_obj', string='Emergency Contact')

    color = fields.Integer('Color Index', compute='change_colore_on_kanban')
    contract_status = fields.Char('Contract State', compute='change_colore_on_kanban')
    contract_state =  fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Running'),
        ('close', 'Expired'),
        ('cancel', 'Cancelled'),
        ('no', 'No Contract'), 
        ], string='Contract Status',default="no")
    
    def change_colore_on_kanban(self):
        """
            Method Used For Kanban
            Color On Based Of Contract Status
        """
        for record in self:
            record.color = 0
            record.contract_status = 'No Contract'
            contract_cancel = self.env['hr.contract'].search([
                ('employee_id','=',record.id),
                ('state','=','cancel')],limit=1)
            if contract_cancel:
                record.color=5
                record.contract_status = 'Contract Cancelled'
            contract_close = self.env['hr.contract'].search([
                ('employee_id','=',record.id),
                ('state','=','close')],limit=1)
            if contract_close:
                record.color = 0
                record.contract_status = 'Contract Expired'
            contract_open = self.env['hr.contract'].search([
                ('employee_id','=',record.id),
                ('state','=','open')],limit=1)
            if contract_open:
                record.color = 3
                record.contract_status = 'Contract Running'
            contract_draft = self.env['hr.contract'].search([
                ('employee_id','=',record.id),
                ('state','=','draft')],limit=1)
            if contract_draft:
                record.color = 2
                record.contract_status = 'Contract Draft'

    @api.onchange('is_check')
    def onchange_is_check (self):
        """
        """
        if self.is_check == True:
            self.city= self.p_city
            self.state_id= self.p_state_id
            self.country_id= self.p_country_id
            self.street= self.p_street
            self.street2= self.p_street2
            self.zip= self.p_zip
        else:
            self.city= False
            self.state_id= False
            self.country_id= False
            self.street= False
            self.street2= False
            self.zip= False



    @api.model
    def create(self, vals):
        if vals.get('emp_seq',  '/') == '/':
            vals['emp_seq'] = self.env['ir.sequence'].next_by_code(
                'hr.employee') or '/'
        return super(Employee, self).create(vals)
        
        
    def copy(self, default=None):
        if default is None:
            default = {}
        default['emp_seq'] = '/'
        return super(Employee, self).copy(default=default)
    
    def _compute_dateofjoin(self):
        if self.joining_date:
            date_today = datetime.today().strftime('%Y-%m-%d')
            day_from = fields.Datetime.from_string(self.joining_date)
            day_to = fields.Datetime.from_string(date_today)
            nb_of_days = (day_to - day_from).days + 1
            difference_in_years = relativedelta(day_to, day_from).years
            if (difference_in_years < 1):
                self.totalyears = str(nb_of_days) + 'days (' + str(relativedelta(day_to, day_from).months) + ')'
            else:
                self.totalyears = str(difference_in_years) + ' years'

class WorkPermit(models.Model):
    _name = 'work.permit'
    _description = 'WorkPermit'

    visa_no = fields.Char('Visa No')
    workpermit_no = fields.Char('Work Permit No')
    visa_expiry_date = fields.Char('Visa Expiry Date')
    employee = fields.Many2one('hr.employee')