# -*- coding: utf-8 -*-

from . import models
from . import hr_passport
from . import hr_nationality_detail
from . import hr_employee_banks
from . import hr_empdoc
from . import hr_emergency
from . import hr_education
from . import hr_employee

