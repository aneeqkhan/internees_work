# -*- coding: utf-8 -*-

from odoo import _, api, fields, models


class HrBanks(models.Model):
    _name = 'hr.banks'
    _description = "Employee Banks"
    _rec_name = "acc_number"

    acc_number = fields.Char("Account Number",required=True)
    bank_name = fields.Char("Bank Name")
    employee = fields.Many2one('hr.employee', string="Employee")
    title = fields.Char("Title of Account")
    swift_code = fields.Char("Swift Code")

    _sql_constraints = [('acc_number_uniq', 'unique (acc_number)',
                         'Account Number is already existed!')]

    @api.constrains('employee')
    def place_employee_bank(self):
        _ = self.env['hr.employee'].search([('id', '=', self.employee.id)]).update(
            {'bank_id': self.id})