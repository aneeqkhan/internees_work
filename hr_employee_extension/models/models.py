
from odoo import api, fields, models, _
import logging
from odoo.exceptions import UserError, ValidationError
from datetime import datetime,date, timedelta
from dateutil.relativedelta import relativedelta

class HrEmployeeContractName(models.Model):
    _name = 'hr.dependent'

    name = fields.Char(string='Dependent Name', help='Name')
    
    relation = fields.Selection([
        ('father', 'Father'),
        ('mother', 'Mother'),
        ('daughter', 'Daughter'),
        ('son', 'Son'),
        ('brother', 'Brother'),
        ('husband','Husband'),
        ('sister', 'Sister'),
        ('cousin', 'Cousin'),
        ('uncle', 'Uncle'),
        ('wife', 'Wife')], 
        string='Relation', help='Relation')
    #relation = fields.Char(string='Relation', help='Relation with employee')
    employee_obj = fields.Many2one('hr.employee')
    dob = fields.Date("DOB")
    age = fields.Integer("Age")
    
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ],  default="male")

    cnic = fields.Char("CNIC")
    
    @api.onchange('dob')
    def _onchange_birth_date(self):
        if self.dob:
            d1 = datetime.strptime(str(self.dob), "%Y-%m-%d").date()
            d2 = date.today()
            self.age = relativedelta(d2, d1).years



class WorkLocation(models.Model):
    _name='hr.employee.work.location'

    name = fields.Char('Address') 

class EmployeeReligion(models.Model):
    _name='hr.employee.religion'

    name = fields.Char('Religious name')  

class Department(models.Model):
    _inherit = "hr.department"
    
    branch_id = fields.Many2one('res.branch', 'Branch', default=lambda self: self.env.user.default_branch_id)


class HRJOB(models.Model):
    _inherit = "hr.job"
    
    grade_id = fields.Many2one('grade.type', string="Grade")
    description = fields.Html("Description")
    branch_id = fields.Many2one('res.branch', 'Branch', default=lambda self: self.env.user.default_branch_id)
    
class GradeType(models.Model):
    _name = "grade.type"
    
    name = fields.Char('Grade')
    branch_id = fields.Many2one('res.branch', 'Branch', default=lambda self: self.env.user.default_branch_id)
    company_id = fields.Many2one('res.company',string="Company")

class Companies(models.Model):
    
    _name = "companies"
    _order = 'name'
    
    name = fields.Char("Company Name")
    comp_type = fields.Selection([('same', 'Same'),('other', 'Other')], string='Type', default='other')
    

class Experiance(models.Model):
    
    _name = "hr.employee.experiance"
    _order = 'exp_st_date desc, exp_nd_date desc'
   
    company_name = fields.Many2one("companies","Company Name")
    exp_job_id = fields.Char(string='Job Position')
    exp_st_date = fields.Date('Start Date')
    exp_nd_date = fields.Date('End Date')
    exp_attachment_id = fields.Many2many('ir.attachment', 'exp_attachment_rel', 'exp_ref', 'attach_ref',
                                        string="Attachment", help='You can attach the copy of your Experiance Certificate')
    employee_ref = fields.Many2one(string="Employee", comodel_name='hr.employee',invisible=1)
    total_exp_dur = fields.Integer(string="Duration(months)")
    dept_id = fields.Many2one('hr.department', 'Department')
    remarks = fields.Text('Remarks')

    @api.onchange('exp_st_date', 'exp_nd_date', 'total_exp_dur')
    def calculate_date_hour(self):
        if self.exp_st_date and self.exp_nd_date:
            d1=datetime.strptime(str(self.exp_st_date),"%Y-%m-%d") 
            d2=datetime.strptime(str(self.exp_nd_date),"%Y-%m-%d")
            d3=d2-d1
            self.total_exp_dur = (d3.days) // 30

class EmpSkills(models.Model):
    
    _name = "employee.skill"
    name = fields.Char('Name')
    employee_id = fields.Many2one('hr.employee', 'Employee')


class DegreeProgram(models.Model):
    
    _name = "degreeprogram"
    _order = 'name'
    
    name = fields.Char("Degree Program")

class Institutes(models.Model):
    
    _name = "institute"
    _order = 'name'
    
    name = fields.Char("Institute Name")


class DegTitle(models.Model):
    
    _name = "degree.title"
    _order = 'name'
    
    name = fields.Char("Degree Title")

class Qualifications(models.Model):
    
    _name = "hr.employee.qualification"
    _order = 'deg_st_date desc, deg_nd_date desc'

    degree_prgrm = fields.Many2one("degreeprogram",string="Specialization/Subject")
    institute_name = fields.Many2one("institute",string="Institute Name")
    degree_name = fields.Many2one('degree.title', string='Certificate/Degree')
    deg_st_date = fields.Date('Start Date')
    deg_nd_date = fields.Date('End Date')
    deg_percentage = fields.Char('%age/CGPA/Grade/Division')
    employee_ref = fields.Many2one(string="Employee", comodel_name='hr.employee',invisible=1)
    total_exp_dur = fields.Integer(string="Duration(months)")

 
class CertificateTitle(models.Model):
    
    _name = "certificatetitle"
    _order = 'name'
    
    name = fields.Char("Certificate Title")

class Certificate(models.Model):
    
    _name = "hr.employee.certificate"
    _order = 'crtfct_st_date desc, crtfct_nd_date desc'

    crtfct_title = fields.Many2one("certificatetitle","Certificate/Diploma")
    institute_name = fields.Many2one("institute",string="Institute-Name")
    crtfct_st_date = fields.Date('Start Date')
    crtfct_nd_date = fields.Date('End Date')
    crtfct_percentage = fields.Char("%age/CGPA/Grade/Division")
    crtf_attachment_id = fields.Many2many('ir.attachment', 'crtf_attachment_rel', 'crtf_ref', 'attach_ref',string="Attachment")
    employee_ref = fields.Many2one(string="Employee", comodel_name='hr.employee', invisible=1)
    crtfct_field = fields.Char('Subject/Specialization')
    total_exp_dur = fields.Integer(string="Duration(months)")

    @api.onchange('crtfct_st_date', 'crtfct_nd_date', 'total_exp_dur')
    def calculate_hour(self):
        if self.crtfct_st_date and self.crtfct_nd_date:
            d1=datetime.strptime(str(self.crtfct_st_date),"%Y-%m-%d") 
            d2=datetime.strptime(str(self.crtfct_nd_date),"%Y-%m-%d")
            d3=d2-d1
            self.total_exp_dur = (d3.days) // 30


   

class domicile(models.Model):
    _name = "hr.domicile"
    name = fields.Char("Name")


class ResPartnerBankDetails(models.Model):
    _inherit = 'res.partner.bank'

    res_partner_ref = fields.Many2one('hr.employee',"Employee")
    branch_code = fields.Char(string='Branch Code')
    acc_title = fields.Char(string='Account Title')
    IBAN = fields.Char(string='IBAN')