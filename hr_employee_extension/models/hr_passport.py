# -*- coding: utf-8 -*-

from odoo import _, api, fields, models, _
from odoo.exceptions import UserError, ValidationError



class HrPassport(models.Model):
    _name = 'hr.passport'
    _description = "Passport"
    _rec_name = "passport_number"

    employee = fields.Many2one('hr.employee', string="Employee")
    passport_name = fields.Char("Name as Passport")
    passport_number = fields.Char("Passport Number",required=True)
    expiry_date = fields.Date("Passport Expiry date")
    is_employee = fields.Boolean("Is a Employee")
    passport_issue_country = fields.Char("Passport issue country")
    passport_issue_place = fields.Char("Passport issue Place")
    passport_status = fields.Selection([('we', 'With Employee'), ('wc', 'With Company')], string="Passport Status")
    is_dependent = fields.Boolean("Is a Dependent")
    d_passport_name = fields.Char("Dependent Name as Passport")
    d_emp = fields.Many2one('hr.employee')

    @api.onchange('is_dependent','employee')
    def _onchange_is_dependent(self):
        if self.is_dependent:
            if self.employee:
                self.d_emp = self.employee
            self.is_employee = False

    @api.onchange('is_employee')
    def check_boolean(self):
        if self.is_employee:
            self.passport_name = self._context.get('default_passport_name')
            self.is_dependent = False
            self.d_emp = False
        else:
            self.passport_name = ''

    @api.constrains('is_dependent','is_employee')
    def check_passport_type_type(self):
        if not self.is_employee and not self.is_dependent:
            raise UserError(_('Select the Checkbox is a Employee or is a Dependent'))

    @api.constrains('employee')
    def place_employee_passport(self):
        pas = self.env['hr.passport'].search([('employee', '=', self.employee.id),('is_employee', '=', True)])
        if pas:
            if len(pas) > 1:
                raise UserError(_('This Employee Already Has Passport information !'))
            else:
                __ = self.env['hr.employee'].search([('id', '=', self.employee.id)]).update(
                    {'passport': self.id})


    _sql_constraints = [('passport_number_uniq', 'unique (passport_number)',
                         'Passport Number is already existed!')]
