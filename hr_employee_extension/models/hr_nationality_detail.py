# -*- coding: utf-8 -*-

import re

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class HrNationalId(models.Model):
    _name = 'hr.nationality'
    _description = "National IDs"
    _rec_name = "nationality_name"

    id = fields.Many2one('hr.id.type',string="ID Type")
    nationality_name = fields.Char('ID No.',required=True)
    employee = fields.Many2one('hr.employee',string="Employee",required=True)
    issue_date = fields.Date("Issue date")
    expiry_date = fields.Date("Expiry date")
    date_of_birth = fields.Date("Date of Birth")
    place_of_issue = fields.Char("Place of Issue")
    department = fields.Many2one('hr.department',string="Department")
    blood_group = fields.Selection([
        ('A+','A+'),
        ('B+','B+'),
        ('A-','A-'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),

    ],string="Blood Group")
    dependent = fields.Boolean("Dependent")
    
    # @api.onchange('nationality_name')
    # def national_id_regex(self):
    #     if(self.nationality_name):
    #         result = re.match('^(1)[0-9]?', self.nationality_name, flags=0)
    #         if(len(self.nationality_name) != 10):
    #             raise UserError(_('Invalid Entry. Entry should contain complete 10 numbers'))
    #         if result:
    #             print("Match")
    #         else:
    #             print("Failed")
    #             raise UserError(_('Invalid National ID Entry. Entry should start from 1'))

    

    # _sql_constraints = [('nationality_name_uniq', 'unique (nationality_name)',
    #                      'National ID is already existed!')]


class IdType(models.Model):
    _name = 'hr.id.type'
    _description = "ID Type"

    name = fields.Char("ID Type")
