from odoo import models , fields
import logging

class CrmReport(models.TransientModel):
   _name = 'purchase.reports'
   _description = 'purchase reports'

   partner_ids = fields.Many2many(comodel_name='res.partner' , string='Vendor')

   def action_print_xlsx_report(self):
      data={
         'model':'purchase.reports',
         'form':self.read()[0]
      }
      return self.env.ref('purchase_reports.purchase_reports_xlsx').report_action(self,data=data)
    


