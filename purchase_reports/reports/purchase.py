from datetime import datetime, date, timedelta
import logging
import base64
import io
from typing import NoReturn
from odoo import models

class PurchaseReportXlsx(models.AbstractModel):
    _name = 'report.purchase_reports.purchase_reports_xls'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, purchase_reports):
        # print("hello from purchases")

        #company
        company = self.env.company

        # user
        user = self.env.user
            
        #Design
        heading = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#C0C0C0',
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        seial_number = workbook.add_format({
            "border_color": "#ABABAB",
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        printed = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'black',
            "bg_color": '#C0C0C0',
        })
        worksheet = workbook.add_worksheet('Purchase Report')
        worksheet.set_column('A:U',25)

        row = 1
        col = 0

        # Main heading of company data
        worksheet.merge_range('D1:G2', company.name, company_data)
        worksheet.merge_range('D3:G3', company.street ,company_data)
        worksheet.merge_range('D4:G4', company.email, company_data)
        worksheet.merge_range('D5:G5',company.phone ,company_data)

        #heading 
        worksheet.write('A10','No.',heading)
        worksheet.write('B10','Vendor',heading)
        worksheet.write('C10','Order Date',heading)
        worksheet.write('D10','Vendor Reference',heading)
        # worksheet.write('E10','Purchase Agreement',heading)
        # worksheet.write('F10', 'Source Document',heading)
        worksheet.write('E10', 'Product', heading)
        worksheet.write('F10','Description',heading)
        worksheet.write('G10','Scheduled Date',heading)
        worksheet.write('H10','Quantity',heading)
        worksheet.write('I10','Unit Price',heading)
        worksheet.write('J10','Taxes',heading)
        worksheet.write('K10','Sub Total',heading)
        worksheet.write('L10','Untaxed Amount',heading)
        worksheet.write('M10','Taxes',heading)
        worksheet.write('N10','Total',heading)
        worksheet.write('O10','Receipt Date',heading)
        worksheet.write('P10','Purchase Representative',heading)
        worksheet.write('Q10','Incoterm',heading)
        worksheet.write('R10','Payment Terms',heading)
        worksheet.write('S10','Fiscal Position',heading)
        worksheet.write('T10','State',heading)


        # domain
        domain = []
        if data['form']['partner_ids']:
            domain.append(('partner_id', 'in', data['form']['partner_ids']))       
        purchase_reports = self.env['purchase.order'].search(domain)    

        row = 9
        s_no =  1
        for purchase in purchase_reports:
            row += 1
            worksheet.write(row, 0,s_no,seial_number)
            worksheet.write(row,col + 1, purchase.partner_id.name if purchase.partner_id.name else 0 ,center)
            worksheet.write(row,col + 2, str(purchase.date_order if purchase.date_order else 0 ),center)
            worksheet.write(row,col + 3, purchase.partner_ref if purchase.partner_ref else 0 ,center)
            # worksheet.write(row,col + 4, purchase.requisition_id.name if purchase.requisition_id.name else 0 ,center)
            # worksheet.write(row,col + 5, purchase.origin if purchase.origin else 0 ,center)
            #,,,,,,,,,,,,,,,,,,,,,,,,,,,
            worksheet.write(row,col + 11, purchase.amount_untaxed if purchase.amount_untaxed else 0 ,center)
            worksheet.write(row,col + 12, purchase.amount_tax if purchase.amount_tax else 0 ,center)
            worksheet.write(row,col + 13, purchase.amount_total if purchase.amount_total else 0 ,center)
            # ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            worksheet.write(row,col + 14, str(purchase.date_planned if purchase.date_planned else 0 ),center)
            worksheet.write(row,col + 15, purchase.user_id.name if purchase.user_id.name else 0 ,center)
            worksheet.write(row,col + 16, purchase.incoterm_id.name if purchase.incoterm_id.name else 0 ,center)
            worksheet.write(row,col + 17, purchase.payment_term_id.name if purchase.payment_term_id.name else 0 ,center)
            worksheet.write(row,col + 18, purchase.fiscal_position_id if purchase.fiscal_position_id else 0 ,center)
            worksheet.write(row,col + 19, purchase.state if purchase.state else 0 ,center)

            for lines in purchase.order_line:
                worksheet.write(row,col + 4, lines.product_id.name if lines.product_id.name else 0 ,center)
                worksheet.write(row,col + 5, lines.name if lines.name else 0 ,center)
                worksheet.write(row,col + 6, str(lines.date_planned if lines.date_planned else 0 ),center)
                worksheet.write(row,col + 7, lines.product_qty if lines.product_qty else 0 ,center)
                worksheet.write(row,col + 8, lines.price_unit if lines.price_unit else 0 ,center)
                worksheet.write(row,col + 9, lines.taxes_id.name if lines.taxes_id.name else 0 ,center)
                worksheet.write(row,col + 10, lines.price_subtotal if lines.price_subtotal else 0 ,center)
                row += 1
            s_no += 1

        worksheet.write(row + 2, 0, "Printed by: ", printed)
        worksheet.write(row + 2, 1, user.name, printed) 