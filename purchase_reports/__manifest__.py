{
    'name': 'Purchase Reports',
    'author' : 'Romayyah',
    'version': '1.1',
    'category': 'Purchase',
    'summary': 'Generate excel and pdf reports for purchases',
    'description': "Purchase Reports",
    'depends': [
        'base',
        "purchase",
        "report_xlsx",   
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_reports.xml',
        'reports/pdf_purchase.xml',
        'reports/report.xml',
    ],   
}