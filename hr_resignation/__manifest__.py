
{
    'name': 'Resignation/Termination',
    'version': '1.1',
    'summary': 'Handle the resignation and termination process of the employee',
    'depends': ['hr',  'mail'],
    'category': 'Generic Modules/Human Resources',
    'maintainer': 'Aneeq Ul Haq',
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'data/termination_reasons.xml',
        'data/resignation_reasons.xml',
        'wizard/forward_wizard.xml',
        'views/resignation_view.xml',
        'views/hr_termination.xml',
        'views/views.xml',
        'views/resignation_request_mail_tamplate.xml',
        # 'reports/pdf_resignations.xml',
        'reports/pdf_termination.xml',
        # 'reports/pdf.xml',
    ],
}

