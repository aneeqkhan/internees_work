
from odoo import models, fields, api, _
from odoo.exceptions import Warning, UserError
from datetime import datetime,date


class ImportSaleforecastLine(models.TransientModel):
    _name = 'forwardresignation.wizard'
    
    def _get_resignation(self):
        resignation = None
        context = dict(self._context or {})
        if len(context.get('active_ids')) == 1:
            resignation = self.env['hr.resignation'].browse(context.get('active_ids'))
            return resignation.id
        return resignation

    name = fields.Html(string='Comment')
    manager_id = fields.Many2one(comodel_name='hr.employee',domain=lambda self: self._get_forward_to_domain(), string='Forward To',required=True)
    hr_resignation = fields.Many2one(comodel_name="hr.resignation", default=_get_resignation)
    current_user = fields.Boolean(string='Logged In User', compute="_get_current_user")


    @api.onchange('hr_resignation')
    def _get_forward_to_domain(self):
        manager_ids = []
        
        #getting employee personal managers id
        if self.hr_resignation.employee_id and self.hr_resignation.employee_id.parent_id:
            employee_manager_id = self.hr_resignation.employee_id.parent_id.id
            manager_ids.append(employee_manager_id)
        else:
            employee_manager_id = False
        #getting employee department manager id and all parent department ids
        if self.hr_resignation.employee_id and self.hr_resignation.employee_id.department_id:
            department_ids = self.env['hr.department'].search([('id','parent_of',self.hr_resignation.employee_id.department_id.id)])
            dept_manager_ids = department_ids.mapped('manager_id')
        else:
            dept_manager_ids = False
        if dept_manager_ids:
            for manager in dept_manager_ids:
                manager_ids.append(manager.id)
        #removing employee id so that he/she cant forward it to him/herself
        if manager_ids and self.hr_resignation.employee_id.id in manager_ids:
            manager_ids.remove(self.hr_resignation.employee_id.id)
        if manager_ids:
            logged_in_emp = self.env['hr.employee'].search([('user_id','=',self.env.user.id)],limit=1)
            if logged_in_emp and logged_in_emp.id in manager_ids:
                manager_ids.remove(logged_in_emp.id)
        if len(manager_ids) > 0:
            return {'domain': {'manager_id' : [('id', 'in', manager_ids)]}}
        else:
            return {'domain': {'manager_id' : [('id', '=', False)]}}
    
    def _get_current_user(self):
        for rec in self:
            if rec.manager_id and rec.manager_id.user_id:
                if rec.manager_id.user_id.id == self.env.user.id:
                    rec.current_user = True
    
    def action_resign_forward(self):
        if self.manager_id:
            self.hr_resignation.manager_id = self.manager_id.id
