from odoo import models, fields, api, _


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    
    resign_date = fields.Date('Resign Date', readonly=True, help="Date of the resignation")
    resigned = fields.Boolean(
        string="Resigned", default=False, store=True,
        help="If checked then employee has resigned")
    fired = fields.Boolean(string="Terminated Employees", default=False, store=True, 
    help="If checked then employee has fired")