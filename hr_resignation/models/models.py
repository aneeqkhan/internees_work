from datetime import datetime, timedelta,date
from odoo import models, fields, api, _


class hr_resignation_and_termination(models.Model):
    _name="resignation.reasons"

    def _default_resignation_type (self):
        default_resignation_type = self._context.get('default_resignation_type') 
        if default_resignation_type == 'resigned':
            return 'resigned'
        if default_resignation_type == 'fired':
            return 'fired'
    name=fields.Char(String="Resignation Reasons")
    resignation_type = fields.Selection(selection=[('resigned', 'Resignation'),('fired', 'Termination')],store=True,
        default=lambda self: self._default_resignation_type(),
        string="Request Type",
        help="Select the type of request: Resignation OR Termination")


class NumberOfDays(models.Model):
    _name = 'hr.resignation.days'
   
    name = fields.Char(string='Name')
    number_of_days = fields.Integer(string="Number of days")

    @api.model
    def create(self, vals):
        vals['name'] = "Days: " + str(vals['number_of_days'])
        return super(NumberOfDays, self).create(vals)


class TerminateEmployee(models.Model):
    _name = "terminate.employee"

    emp_id = fields.Many2one(comodel_name='hr.employee', string='Employee' )


class TerminationDate(models.Model):

    _inherit = "hr.contract"

    termination_date = fields.Datetime(string='Termination Date',readonly=True)
    is_terminated = fields.Boolean('Is Terminated')

class terminationRule(models.Model):
    _name = 'termination.rules'

    name = fields.Text(string='Reason', required=True)
    type_id = fields.Many2one(comodel_name='termination.type', string='Termination Types')


class terminationType(models.Model):

    _name = 'termination.type'
    
    name = fields.Char(string='Termination Type')
    rules_id = fields.One2many(comodel_name='termination.rules', inverse_name='type_id', string='Termination Rules')
    action = fields.Many2many(comodel_name='hr.termination.action', string='Action')
    notice_period = fields.Integer(string='Notice Period')
    having_notice = fields.Boolean(string='Having Notice Period')
    


class isTerminated(models.Model):
    _inherit = "hr.employee"

    is_terminated = fields.Boolean(string='Is Terminated')
    termination_id = fields.Many2one(comodel_name='termination.rules', string='Warning Reason')
    termination_type = fields.Many2one(comodel_name='termination.type', string='Warning Type')
    warn_type = fields.Selection([
        ('limited', 'Limited'), 
        ('unlimited', 'Unlimited'),
        ('no_warn', 'No Warning')], string='Warning type', default='limited')
    warning_limit = fields.Integer(string='Warning Limit', default="3")
    warning_count = fields.Integer(string='Warning Limit')
    warning_hist = fields.One2many(comodel_name='hr.resignation', inverse_name='employee_id', string='Warnings')
    
    @api.onchange('warn_type')
    def update_warn_limit(self):
        if self.warn_type == 'no_warn':
            self.warning_limit = 0
        elif self.warn_type == 'unlimited':
            self.warning_limit = -1
        else: pass

   

# to add action into the terminate employee
class TerminateAction(models.Model):
    _name = 'hr.termination.action'

    name = fields.Char(string='Action')
    is_taken = fields.Boolean(string='Is Taken', default=False)
    employee_id = fields.Many2one(comodel_name='termination.wizard', string='Employee')


class WarningLetter(models.Model):
    _name = "warning.letter"
    
    name = fields.Char(string='Warning')


 

class WarningLimit(models.TransientModel):
    _name = "warning.limit"

    name = fields.Char('Description')
    warn_type = fields.Selection([
        ('limited', 'Limited'), 
        ('unlimited', 'Unlimited'),
        ('no_warn', 'No Warning')], string='Warning type', 
    default='limited')
    warn_limit = fields.Integer(string='Warning Limit', default=1)
    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')

    @api.onchange('warn_type')
    def update_warn_limit(self):
        if self.warn_type == 'no_warn':
            self.warn_limit = 0
        elif self.warn_type == 'unlimited':
            self.warn_limit = -1
        else: pass

    
    def set_warning_limit(self):
        for rec in self.env['hr.employee'].search([]):
            if self.warn_type == 'unlimited':
                rec.warn_type = 'unlimited'
                rec.warning_limit = -1
            elif self.warn_type == 'limited':
                rec.warn_type = 'limited'
                if self.warn_limit != rec.warning_limit:  
                    rec.warning_limit = self.warn_limit
                else:
                    rec.warning_limit = self.warn_limit
            else:
                rec.warn_type = 'no_warn'
                rec.warning_limit = 0
        return True

class otherinfo(models.Model):
    
    _name='other.information'

    other_info=fields.Many2one('termination.wizard',string="other")
    action=fields.Char(string="Action")
    forward_to = fields.Many2one('hr.employee', string='Forwarded to', readonly='1')
    responsible=fields.Many2one('res.users',string="Responsible")
    description=fields.Html(string="Description")
    current_date = fields.Datetime(string='Date', default=datetime.now())


class ProcessDetails(models.Model):
    _name = 'process.detail'

    name = fields.Selection([
        ('inq_rec', 'Recommendation For Inquiry'),
        ('inq', 'Inquiry'),
        ('appr_rec', 'Recommendation For Approval'),
        ('appr', 'Approval')], string='Process', default='inq_rec', readonly='1')

    rec_to = fields.Many2one('hr.employee', string='Recommended to', readonly='1')
    forward_to = fields.Many2one('hr.employee', string='Forwarded to', readonly='1')
    approver = fields.Many2one('hr.employee', string='Approver', readonly='1')
    job_id = fields.Many2one('hr.job', string='Job Position', readonly='1', store=True)
    dept_id = fields.Many2one('hr.department', string='Department', readonly='1', store=True)
    com_chairman = fields.Many2one('res.users', 'Committee Chairman', readonly='1')
    com_members = fields.Many2many('res.users', string='Committee Members', readonly='1')
    comments = fields.Html('Description', readonly='1')
    term_id = fields.Many2one('termination.wizard', 'Warning/Termination Ref.', readonly='1')