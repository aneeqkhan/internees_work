import logging
import datetime
from datetime import datetime, timedelta,date
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from email.utils import formataddr


date_format = "%Y-%m-%d"


class HrResignation(models.Model):
    _name = 'hr.resignation'
    _inherit = 'mail.thread'
    _rec_name = 'employee_id'


    def _default_resignation_type (self):
        default_resignation_type = self._context.get('default_resignation_type') 
        if default_resignation_type == 'resigned':
            return 'resigned'
        if default_resignation_type == 'fired':
            return 'fired'
    
    def _get_default_reason(self):
        default_resignation_type = self._context.get('default_resignation_type') 
        if default_resignation_type == 'resigned':
            reasons = self.env['resignation.reasons'].search([('resignation_type','=','resigned')],limit=1)
            return reasons
        if default_resignation_type == 'fired':
            reasons = self.env['resignation.reasons'].search([('resignation_type','=','fired')],limit=1)
            return reasons
        
    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, 
    index=True,default=lambda self: _('New'))
    employee_id = fields.Many2one('hr.employee', string="Employee", default=lambda self: self.env.user.employee_id.id,
        help='Name of the employee for whom the request is creating')
    department_id = fields.Many2one('hr.department', string="Department", 
        related='employee_id.department_id',help='Department of the employee')
    
    resign_confirm_date = fields.Date(default=date.today(),string="Confirm Date",
        help='Date on which the request is confirmed by the employee.',track_visibility="always")
    approved_revealing_date = fields.Date(string="Approved Date", readonly=True,
        help='Date on which the request is confirmed by the manager.',track_visibility="always")
    joined_date = fields.Date(string="Joining Date", required=False, readonly=True, 
        related="employee_id.joining_date", help='Joining date of the employee.i.e Start date of the first contract')
    expected_revealing_date = fields.Date(default=date.today(),string="Last Day", required=True,
        help='Employee requested date on which he is revealing from the company.')
    
    reason = fields.Text(string="Reason",  help='Specify reason for leaving the company')
    notice_period = fields.Char(string="Notice Period")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('approved', 'Approved'),
        ('cancel', 'Cancelled')
    ],string='Status', default='draft', track_visibility="always")

    resignation_type = fields.Selection(selection=[
        ('resigned', 'Resignation'),
        ('fired', 'Termination')],store=True,
        default=lambda self: self._default_resignation_type(),
        string="Request Type",
        help="Select the type of request: Resignation OR Termination")

    read_only = fields.Boolean(string="check field")
    employee_contract = fields.Char(String="Contract",store="True")
    resignation_reasons = fields.Many2one(comodel_name='resignation.reasons',
        default=lambda self: self._get_default_reason(), 
        string='Reason',required = True)
    manager_id = fields.Many2one('hr.employee', string='Forward To', store="True", track_visibility="always")

    waiver_notice_period = fields.Boolean(default=False, string="Waiver of notice period")
    resignation_days_id = fields.Many2one("hr.resignation.days","Number of days")

    @api.onchange('resign_confirm_date','resignation_days_id','waiver_notice_period')
    def get_last_working_day(self):
        if self.resign_confirm_date and self.resignation_days_id and self.waiver_notice_period:
            result = fields.Date.from_string(self.resign_confirm_date) + timedelta(days=int(self.resignation_days_id.number_of_days) )
            if result:
                self.expected_revealing_date = result
        else:
            self.expected_revealing_date = date.today()


    # Termination works
    def pendingtermination(self):
        count =self.search_count([('state','=','issued'),('user_id','=',self.env.user.id)])
        return count

    def set_default_user(self):
        current_user = self.env.uid
        if current_user:
            return self.env['hr.employee'].search([('user_id', '=', current_user)], limit=1)

    
    count = fields.Integer(string='Warnings Received', related='employee_id.warning_count')
    limit = fields.Integer(string='Warning Limit', related='employee_id.warning_limit')

    above_warning_limit = fields.Boolean(string='Above Limit',default=False)
    check_warn_limit_visible = fields.Boolean(string='Check Limit',default=False)
    created_by = fields.Many2one(comodel_name='hr.employee', string='Created By', default=set_default_user)
    termination_reason = fields.Many2one(comodel_name='termination.rules', string='Reason')
    current_date = fields.Date(string='Date', default=date.today())
    Termination_date = fields.Date(string='Termination Date')
    notice_date = fields.Date(string='Notice Date', store=True)
    termination_type = fields.Many2one(comodel_name='termination.type',string='Termination Type')
    notice_period = fields.Integer(string='Notice Period')
    having_notice = fields.Boolean(string='Having Notice Period')
    termination_action_id = fields.Many2many(comodel_name='hr.termination.action', string='Action')
    button_show = fields.Boolean(string='', default=False)
    warning = fields.Many2one(comodel_name='warning.letter', string='Subject')
    user_id = fields.Many2one('res.users', string='User',related="employee_id.user_id")
    sel_action = fields.Selection(string='Select Action', selection=[ 
        ('warning', 'Warning'),
        ('terminate', 'Termination')],
        required=True, default='warning')
    process_detail_id = fields.One2many('process.detail', 'term_id', string='Process Details')
    rec_to_manager = fields.Boolean('Recommended to manager for inquiry?', default=False)
    other_information=fields.One2many('other.information','other_info' ,string="Other Info")
    contract_end_date=fields.Date("Contract_end_date")
    html_text = fields.Text(string='Comment')
    remarks = fields.Html(string='Comment')

    @api.onchange('termination_type')
    def set_notice_date(self):
        if self.termination_type.having_notice == True:
            self.notice_date = datetime.now() + timedelta(days=self.termination_type.notice_period)
        else:
            self.notice_date = datetime.now()
    

    @api.onchange('Termination_date','notice_date')
    def set_Termination_date(self):
        for rec in self:
            if self.Termination_date and self.notice_date:
                if rec.Termination_date < rec.notice_date:
                    raise ValidationError('Termination date must be greater than notice date')

    @api.onchange('sel_action','name')
    def check_warning_limit(self):
        if self.name and self.resignation_type == 'fired' :
            if self.count >= self.employee_id.warning_limit:
                self.check_warn_limit_visible = False
                self.above_warning_limit = True
                if self.sel_action == 'terminate':
                    self.above_warning_limit = True
                    self.check_warn_limit_visible = False
                else:
                    self.above_warning_limit = True
                    self.check_warn_limit_visible = False
            elif self.count < self.employee_id.warning_limit:
                if self.sel_action == 'terminate':
                    self.above_warning_limit = False
                    self.check_warn_limit_visible = True
                else:
                    self.above_warning_limit = False
                    self.check_warn_limit_visible = False
                
    @api.onchange('termination_reason')
    def _onchange_termination_reason(self):
        if self.termination_reason and self.resignation_type == 'fired':
            self.termination_type = self.termination_reason.type_id
            self.termination_action_id = self.termination_type.action
            self.having_notice = self.termination_type.having_notice
            self.notice_period = self.termination_type.notice_period
            for count in self.termination_action_id:
                count.is_taken = False

  
    def text_from_html(self, html_content, max_words=None, max_chars=None,ellipsis=u"…", fail=False):
        words = u"".join(html_content).split()
        suffix = max_words and len(words) > max_words
        if max_words:
            words = words[:max_words]
        text = u" ".join(words)
        suffix = suffix or max_chars and len(text) > max_chars
        if max_chars:
            text = text[:max_chars - (len(ellipsis) if suffix else 0)].strip()
        if suffix:
            text += ellipsis
        return text

    

    @api.onchange('employee_id')
    def update_sel_action(self):
        """
            Employee Managers
        """
        if self.resignation_type == 'fired':
            if self.limit == 0:
                self.sel_action = 'terminate'
            elif self.limit == self.count:
                self.sel_action = 'terminate'
            else: pass
        manager_ids = []
        #getting employee personal managers id
        if self.employee_id and self.employee_id.parent_id:
            employee_manager_id = self.employee_id.parent_id.id
            manager_ids.append(employee_manager_id)
        else:
            employee_manager_id = False
        #getting employee department manager id and all parent department ids
        if self.employee_id and self.employee_id.department_id:
            department_ids = self.env['hr.department'].search([('id','parent_of',self.employee_id.department_id.id)])
            dept_manager_ids = department_ids.mapped('manager_id')
        else:
            dept_manager_ids = False
        if dept_manager_ids:
            for manager in dept_manager_ids:
                manager_ids.append(manager.id)
        #removing employee id so that he/she cant forward it to him/herself
        logged_in_emp = self.env['hr.employee'].search([('user_id','=',self.env.user.id)],limit=1)
        if manager_ids and logged_in_emp and logged_in_emp.id in manager_ids:
            manager_ids.remove(logged_in_emp.id)
        if manager_ids and self.employee_id.id in manager_ids:
            manager_ids.remove(self.employee_id.id)
        if len(manager_ids) > 0:

            return {'domain': {'manager_id' : [('id', 'in', manager_ids)]}}
        else:
            logging.info("No")
            return {'domain': {'manager_id' : [('id', '=', False)]}}


    # Termination work ended

    @api.onchange('employee_id')
    @api.depends('employee_id')
    def _compute_read_only(self):
        """ Use this function to check weather the user has the permission to change the employee"""
        res_user = self.env['res.users'].search([('id', '=', self._uid)])
        if res_user.has_group('hr.group_hr_user'):
            self.read_only = True
        else:
            self.read_only = False      
        

  
    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('hr.resignation') or _('New')
        res = super(HrResignation, self).create(vals)
        if res.employee_id:
            no_of_contract = self.env['hr.contract'].search([('employee_id', '=', res.employee_id.id)])
            for contracts in no_of_contract:
                if contracts.state == 'open':
                    pass
                # else:
                #     raise ValidationError(_('Employee contract must be in runing state !'))
        return res

    @api.constrains('employee_id')
    def check_employee(self):
        for rec in self:
            if rec.resignation_type == 'resigned':
                if not self.env.user.has_group('hr.group_hr_user'):
                    if rec.employee_id.user_id.id and rec.employee_id.user_id.id != self.env.uid:
                        raise ValidationError(_('You cannot create request for other employees'))

    @api.onchange('employee_id')
    @api.depends('employee_id')
    def check_request_existence(self):
        # Check whether any resignation request already exists
        for rec in self:
            if rec.resignation_type == 'resigned':
                if rec.employee_id:
                    resignation_request = self.env['hr.resignation'].search([
                        ('employee_id', '=', rec.employee_id.id),
                        ('state', 'in', ['confirm', 'approved'])
                    ])
                    if resignation_request:
                        raise ValidationError(_('There is a resignation request in confirmed or'
                        ' approved state for this employee'))
                    
                    if rec.employee_id:
                        no_of_contract = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)])
                        for contracts in no_of_contract:
                            if contracts.state == 'open':
                                rec.employee_contract = contracts.name
                            

    @api.constrains('joined_date')
    def _check_dates(self):
        # validating the entered dates
        for rec in self:
            if rec.resignation_type == 'resigned':
                resignation_request = self.env['hr.resignation'].search([
                    ('employee_id', '=', rec.employee_id.id),
                    ('state', 'in', ['confirm', 'approved'])])
                if resignation_request:
                    raise ValidationError(_('There is a resignation request in confirmed or'
                    ' approved state for this employee'))
    
    
    def _get_default_from(self):
        if self.env.user.email:
            return formataddr((self.env.user.name, self.env.user.email))
        raise UserError(_("Unable to send email, please configure the sender's email address or alias."))

    
    
    def confirm_resignation(self):
        if self.resignation_type == 'resigned':
            if self.joined_date:
                if self.joined_date >= self.expected_revealing_date:
                    raise ValidationError(_('Last date of the Employee must be anterior to Joining date'))
                
                if not self.employee_id.parent_id.work_email:
                    raise ValidationError(_('Please add work email in the employee Manager Profile.'))

                # if not self.employee_id.work_email:
                #     raise ValidationError(_('Please add work email in the Employee Profile'))

                for rec in self:
                    rec.state = 'confirm'
                    self.email_to_manager()
                    rec.resign_confirm_date = str(datetime.now())
            else:
                raise ValidationError(_('Please set joining date for employee'))
        else:
            self.button_show = False
            self.html_text = self.text_from_html(self.remarks)
            msg = self.html_text
            extract_msg = msg[3:-8]
            self.html_text = extract_msg
            for rec in self:
                if self.Termination_date and self.notice_date:
                    if rec.Termination_date < rec.notice_date:
                        raise ValidationError('Termination date must be greater than notice date')
        
            for rec in self:
                rec.state = 'confirm'
                rec.resign_confirm_date = str(datetime.now())


    def cancel_resignation(self):
        for rec in self:
            rec.state = 'cancel'

    def reject_resignation(self):
        for rec in self:
            rec.state = 'cancel'

    def reset_to_draft(self):
        for rec in self:
            rec.state = 'draft'
            rec.employee_id.active = True
            rec.employee_id.resigned = False
            rec.employee_id.fired = False


    def terminate_employee_action(self):
        contracts = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id),('state','in',['open','pending'])])
        
        if not self.Termination_date:
            self.Termination_date = fields.Date.today()
        
        for contract in contracts:
            self.contract_end_date = contract.date_end
            contract.date_end = self.Termination_date
            contract.termination_date = self.Termination_date
            

    def approve_resignation(self):
        no_of_contract = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)])
        current_date = datetime.now()
        today_date = date.today()
        for rec in self:
            if rec.resignation_type == 'resigned':
                if rec.expected_revealing_date and rec.resign_confirm_date:
                    for contracts in no_of_contract:
                        if contracts.state == 'open':
                            #rec.employee_contract = contracts.name
                            rec.state = 'approved'
                else:
                    raise ValidationError(_('Please enter valid dates.'))
            else:
                self.button_show = False
                if self.sel_action == 'terminate':
                    for contracts in no_of_contract:
                        if self.having_notice:
                            contract.is_terminated = True
                            if datetime.strptime(contract.termination_date, '%Y-%m-%d').date() > date.today():
                                raise ValidationError("Employee cannot be terminated before the termination date!")
                        else:
                            contracts.is_terminated = True
                            contracts.termination_date = fields.Date.today()
                            self.employee_id.is_terminated = True
                            self.employee_id.termination_id = self.termination_reason.id
                            self.employee_id.termination_type = self.termination_type.id
                            self.employee_id.active = False

                        if contracts.state == 'open':
                            rec.employee_contract = contracts.name
                            contracts.state = 'cancel'
                            rec.approved_revealing_date = rec.resign_confirm_date
                    
                    if rec.employee_id.active:
                        rec.employee_id.active = False
                        rec.employee_id.resign_date = rec.expected_revealing_date
                        rec.employee_id.fired = True
                        if rec.employee_id.user_id:
                            rec.employee_id.user_id.active = False
                            rec.employee_id.user_id = None

                    self.terminate_employee_action()
                else:
                    self.employee_id.warning_count += 1

                rec.state = 'approved'

                


    def email_to_manager(self):
        MailTemplate = self.env.ref('hr_resignation.resignation_approval_mail_manager', False)
        for rec in self.employee_id:
            if self.employee_id and self.employee_id.parent_id:
                if not self.employee_id.parent_id.work_email:
                    raise ValidationError(_('Work email not set yet on the employee manager profile'))
                else:
                    if self.resignation_type == 'resigned':
                        MailTemplate.write({'email_to': str(self.employee_id.parent_id.work_email),'email_from': str(self.env.user.email)})
                        MailTemplate.send_mail(self.id, force_send=True)
                    else:
                        pass
            else:
                raise ValidationError(_('Please assign Employee Manager !'))    
        return True

       
    
    def email_to_employee(self):
        MailTemplate = self.env.ref('hr_resignation.resignation_approval_mail_employee', False)
        for rec in self.employee_id:
            if not self.employee_id.work_email:
                raise ValidationError(_('Work email not set yet on the Employee  Profile'))
            else:
                if self.resignation_type == 'resigned':
                    MailTemplate.write({'email_to': str(self.employee_id.work_email),'email_from': str(self.env.user.email)})
                    MailTemplate.send_mail(self.id, force_send=True)
                else:
                    pass
        return True
    

    @api.model
    def run_resignation_job(self):
        """
            Cron Job For Contracts
            --
        """
        current_date = datetime.now()
        today_date = date.today()
        resignation_ids = self.env['hr.resignation'].search([('state','=','approved'),('resignation_type', '=', 'resigned')  ])
        if resignation_ids:
            for rec in resignation_ids:
                if rec.expected_revealing_date:
                    if rec.expected_revealing_date == today_date:
                        #resignation.approve_resignation()
                        if rec.expected_revealing_date and rec.resign_confirm_date:
                            no_of_contract = self.env['hr.contract'].search([('employee_id', '=',rec.employee_id.id)])
                            for contracts in no_of_contract:
                                if contracts.state == 'open':
                                    rec.employee_contract = contracts.name
                                    #rec.state = 'approved'
                                    contracts.state = 'close'
                                    rec.approved_revealing_date = rec.resign_confirm_date
                                    self.email_to_employee()
                            # Changing state of the employee if resigning today
                            if rec.employee_id.active:
                            # if rec.expected_revealing_date <= fields.Date.today() and rec.employee_id.active:
                                rec.employee_id.active = False
                                # Changing fields in the employee table with respect to resignation
                                rec.employee_id.resign_date = rec.expected_revealing_date
                                if rec.resignation_type == 'resigned':
                                    rec.employee_id.resigned = True
                                # Removing and deactivating user
                                if rec.employee_id.user_id:
                                    rec.employee_id.user_id.active = False
                                    rec.employee_id.user_id = None
        return True

