# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
import string
from xml.parsers.expat import model
from dateutil.relativedelta import relativedelta
# import pandas as pd
import logging
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils

ROUNDING_FACTOR = 16


class HrLeave(models.Model):
    _inherit = 'hr.leave'
    duration_display = fields.Char('Requested (Days/Hours)', compute='_compute_duration_display', store=True,
                                   help="Field allowing to see the leave request duration"
                                        " in days or hours depending on the leave_type_request_unit")

class Employee(models.Model):
    _inherit = 'hr.employee'

    birthday = fields.Date('Date of Birth', groups="base.group_user", help="Birthday")
    employee_count = fields.Integer(string = 'Total employees')
    department_count = fields.Integer(string = 'Total Departments')
    job_count = fields.Integer(string = 'Total Job positions')

    @api.model
    def check_user_group(self):
        uid = request.session.uid
        user = self.env['res.users'].sudo().search([('id', '=', uid)], limit=1)
        if user.has_group('hr.group_hr_manager'):
            return True
        else:
            return False

    @api.model
    def get_user_employee_details(self):
        uid = request.session.uid
        employee = self.env['hr.employee'].sudo().search_read([('user_id', '=', uid)], limit=1)
        leaves_to_approve = self.env['hr.leave'].sudo().search_count([('state', 'in', ['confirm', 'validate1'])])
        today = datetime.strftime(datetime.today(), '%Y-%m-%d')
        query = """
        select count(id)
        from hr_leave
        WHERE (hr_leave.date_from::DATE,hr_leave.date_to::DATE) OVERLAPS ('%s', '%s') and 
        state='validate'""" % (today, today)
        cr = self._cr
        cr.execute(query)
        leaves_today = cr.fetchall()
        first_day = date.today().replace(day=1)
        last_day = (date.today() + relativedelta(months=1, day=1)) - timedelta(1)
        query = """
                select count(id)
                from hr_leave
                WHERE (hr_leave.date_from::DATE,hr_leave.date_to::DATE) OVERLAPS ('%s', '%s')
                and  state='validate'""" % (first_day, last_day)
        cr = self._cr
        cr.execute(query)
        leaves_this_month = cr.fetchall()
        leaves_alloc_req = self.env['hr.leave.allocation'].sudo().search_count(
            [('state', 'in', ['confirm', 'validate1'])])
        # timesheet_count = self.env['account.analytic.line'].sudo().search_count(
        #     [('project_id', '!=', False), ('user_id', '=', uid)])
        # timesheet_view_id = self.env.ref('hr_timesheet.hr_timesheet_line_search')
        job_applications = self.env['hr.applicant'].sudo().search_count([])
        if employee:
            sql = """select broad_factor from hr_employee_broad_factor where id =%s"""
            self.env.cr.execute(sql, (employee[0]['id'],))
            result = self.env.cr.dictfetchall()
            broad_factor = result[0]['broad_factor']
            if employee[0]['birthday']:
                diff = relativedelta(datetime.today(), employee[0]['birthday'])
                age = diff.years
            else:
                age = False
            if employee[0]['joining_date']:
                diff = relativedelta(datetime.today(), employee[0]['joining_date'])
                years = diff.years
                months = diff.months
                days = diff.days
                experience = '{} years {} months {} days'.format(years, months, days)
            else:
                experience = False
        employee_count = self.env['hr.employee'].sudo().search_count([])
        department_count = self.env['hr.department'].search_count([])
        job_count =self.env['hr.job'].search_count([])
       
        if employee:
            data = {
                'broad_factor': broad_factor if broad_factor else 0,
                'leaves_to_approve': leaves_to_approve,
                'leaves_today': leaves_today,
                'leaves_this_month': leaves_this_month,
                'leaves_alloc_req': leaves_alloc_req,
                # 'emp_timesheets': timesheet_count,
                'job_applications': job_applications,
                # 'timesheet_view_id': timesheet_view_id,
                'experience': experience,
                'age': age,
                'employee_count': employee_count,
                'department_count': department_count,
                'job_count': job_count,    
            }
            employee[0].update(data)
            return employee
        else:
            return False

   
       

    # job_positions per departments
    @api.model
    def get_job_position(self):
        cr = self._cr
        cr.execute("""select department_id, hr_department.name,count(*) 
from hr_job join hr_department on hr_department.id=hr_job.department_id 
group by hr_job.department_id,hr_department.name""")
        dat = cr.fetchall()
        data = []
        for i in range(0, len(dat)):
            data.append({'label': dat[i][1], 'value': dat[i][2]})
        return data

    # employee per departments
    @api.model
    def get_dept_employee(self):
        cr = self._cr
        cr.execute("""select department_id, hr_department.name,count(*) 
from hr_employee join hr_department on hr_department.id=hr_employee.department_id 
group by hr_employee.department_id,hr_department.name""")
        dat = cr.fetchall()
        data = []
        for i in range(0, len(dat)):
            data.append({'label': dat[i][1], 'value': dat[i][2]})
        return data

    
    # monthly join and resign 
    @api.model
    def join_resign_trends(self):
        cr = self._cr
        month_list = []
        join_trend = []
        resign_trend = []
        for i in range(11, -1, -1):
            last_month = datetime.now() - relativedelta(months=i)
            text = format(last_month, '%B %Y')
            month_list.append(text)
        for month in month_list:
            vals = {
                'l_month': month,
                'count': 0
            }
            join_trend.append(vals)
        for month in month_list:
            vals = {
                'l_month': month,
                'count': 0
            }
            resign_trend.append(vals)
        cr.execute('''select to_char(joining_date, 'Month YYYY') as l_month, count(id) from hr_employee 
        WHERE joining_date BETWEEN CURRENT_DATE - INTERVAL '12 months'
        AND CURRENT_DATE + interval '1 month - 1 day'
        group by l_month''')
        join_data = cr.fetchall()
        cr.execute('''select to_char(resign_date, 'Month YYYY') as l_month, count(id) from hr_employee 
        WHERE resign_date BETWEEN CURRENT_DATE - INTERVAL '12 months'
        AND CURRENT_DATE + interval '1 month - 1 day'
        group by l_month;''')
        resign_data = cr.fetchall()

        for line in join_data:
            match = list(filter(lambda d: d['l_month'].replace(' ', '') == line[0].replace(' ', ''), join_trend))
            if match:
                match[0]['count'] = line[1]
        for line in resign_data:
            match = list(filter(lambda d: d['l_month'].replace(' ', '') == line[0].replace(' ', ''), resign_trend))
            if match:
                match[0]['count'] = line[1]
        for join in join_trend:
            join['l_month'] = join['l_month'].split(' ')[:1][0].strip()[:3]
        for resign in resign_trend:
            resign['l_month'] = resign['l_month'].split(' ')[:1][0].strip()[:3]
        graph_result = [{
            'name': 'Join',
            'values': join_trend
        }, {
            'name': 'Resign',
            'values': resign_trend
        }]
        return graph_result

class TotalResignation(models.Model):
    _inherit = 'hr.resignation'       

    @api.model
    def get_total_resignation(self):
        total_resignation = self.env['hr.resignation'].sudo().search_count(['&',('resignation_type','=', 'resigned'),('state', 'in', ['confirm', 'approved'])])
        print(total_resignation)
        return{
            'total_count_resignation':total_resignation    
        }
       
    @api.model
    def get_total_termination(self):
        total_termination = self.env['hr.resignation'].sudo().search_count(['&',('resignation_type','=', 'fired'),('state', 'in', ['confirm', 'approved'])])
        print(total_termination)
        return{
            'total_count_termination':total_termination
        }

class TotalContracts(models.Model):
    _inherit = 'hr.contract'       

    @api.model
    def get_total_contract(self):
        total_contract = self.env['hr.contract'].sudo().search_count([])
        return{
            'all_contract':total_contract
        }

    @api.model
    def get_new_contract(self):
        total_new_contract = self.env['hr.contract'].sudo().search_count([('state','=','draft')])
        return{
            'new_contract' : total_new_contract
        }
    
    @api.model
    def get_running_contract(self):
        total_running_contract = self.env['hr.contract'].sudo().search_count([('state','=','open')])
        return{
            'running_contract' : total_running_contract
        }
    
    @api.model
    def get_expired_contract(self):
        total_expired_contract = self.env['hr.contract'].sudo().search_count([('state','=','close')])
        return{
            'expired_contract' :total_expired_contract
        }
    
    @api.model
    def get_cancel_contract(self):
        total_cancel_contract = self.env['hr.contract'].sudo().search_count([('state','=','cancel')])
        return{
            'cancel_contract' :total_cancel_contract
        }

class SampleSomething(models.Model):
    _inherit = 'res.branch'

    @api.model
    def get_total_branch(self):
        branch_count = self.env['res.branch'].sudo().search_count([])
        return{
            'total_branch_count' :branch_count
        }

     # departments per branch
    @api.model
    def get_branch_dept(self):
        cr = self._cr
        cr.execute("""select branch_id, res_branch.name,count(*) 
from hr_department join res_branch on res_branch.id=hr_department.branch_id 
group by hr_department.branch_id,res_branch.name""")
        dat = cr.fetchall()
        data = []
        for i in range(0, len(dat)):
            data.append({'label': dat[i][1], 'value': dat[i][2]})
        return data
