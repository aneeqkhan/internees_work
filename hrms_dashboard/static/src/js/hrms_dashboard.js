odoo.define('hrms_dashboard.Dashboard', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var ajax = require('web.ajax');
var core = require('web.core');
var rpc = require('web.rpc');
var session = require('web.session');
var web_client = require('web.web_client');
var _t = core._t;
var QWeb = core.qweb;

var HrDashboard = AbstractAction.extend({
    template: 'HrDashboardMain',
    cssLibs: [
        '/hrms_dashboard/static/src/css/lib/nv.d3.css'
    ],
    jsLibs: [
        '/hrms_dashboard/static/src/js/lib/d3.min.js'
    ],
    events: {
        'click .hr_employee' :'hr_employee',
        'click .hr_department':'hr_department',
        'click .hr_job_position':'hr_job_position',
        'click .hr_res_branch ': 'hr_res_branch',    
        'click .hr_contract':'hr_contract',
        'click .hr_resignation':'hr_resignation',
        'click .hr_termination':'hr_termination',
        'click .hr_employee':'hr_employee',
        "click .o_hr_attendance_sign_in_out_icon": function() {
            this.$('.o_hr_attendance_sign_in_out_icon').attr("disabled", "disabled");
            this.update_attendance();
        },
        'click #broad_factor_pdf': 'generate_broad_factor_report',
    },

    init: function(parent, context) {
        this._super(parent, context);
        this.date_range = 'week';  // possible values : 'week', 'month', year'
        this.date_from = moment().subtract(1, 'week');
        this.date_to = moment();
        this.dashboards_templates = ['LoginEmployeeDetails','ManagerDashboard'];
        this.total_branch_count = [];
        this.all_contract = [];
        this.total_count_resignation =[];
        this.new_contract = [];
        this.running_contract = [];
        this.expired_contract = [];
        this.cancel_contract = [];
    },

    willStart: function() {
        console.log("start of function")
        var self = this;
            return self.fetch_data();
    },

    start: function() {
        var self = this;
        this.set("title", 'Dashboard');
        return this._super().then(function() {
            self.update_cp();
            self.render_dashboards();
            self.render_graphs();
            self.$el.parent().addClass('oe_background_grey');
        });
    },

    fetch_data: function() {
        var self = this;
        var def0 =  self._rpc({
                    model: 'hr.employee',
                    method: 'check_user_group'
            }).then(function(result) {
                if (result == true){
                    self.is_manager = true;
                }
                else{
                    self.is_manager = false;
                }
            });
        var def1 =  this._rpc({
                model: 'hr.employee',
                method: 'get_user_employee_details'
        }).then(function(result) {
            self.login_employee =  result[0];
        });
     
        var def2 = self._rpc({
            model: "res.branch",
            method: "get_total_branch",
        })
        .then(function (res) {
            self.total_branch_count = res['total_branch_count'];
        });
        var def3 = self._rpc({
            model: "hr.contract",
            method: "get_total_contract",
        })
        .then(function (res) {
            self.all_contract = res['all_contract'];
        });
        var def4 = self._rpc({
            model: "hr.resignation",
            method: "get_total_resignation",
        })
        .then(function (res) {
            self.total_count_resignation = res['total_count_resignation'];
        });
        var def5 = self._rpc({
            model: "hr.resignation",
            method: "get_total_termination"
        })
        .then(function(res){
            self.total_count_termination = res ['total_count_termination'];
        });
        var def6 = self._rpc({
            model: "hr.contract",
            method: "get_running_contract"
        })
        .then(function(res){
            self.running_contract = res ['running_contract'];
        });
        var def7 = self._rpc({
            model: "hr.contract",
            method: "get_new_contract"
        })
        .then(function(res){
            self.new_contract = res ['new_contract'];
        });
        var def8 =self._rpc({
            model: "hr.contract",
            method: "get_expired_contract"
        }).then(function(res){
            self.expired_contract = res['expired_contract'];
        });
        var def9 =self._rpc({
            model: "hr.contract",
            method: "get_cancel_contract"
        }).then(function(res){
            self.cancel_contract = res['cancel_contract'];
        });
     
        return $.when(def0, def1, def2 , def3 , def4 ,def5 ,def6, def7, def8 , def9);
    },

    render_dashboards: function() {
        var self = this;
        if (this.login_employee){
            var templates = []
            if( self.is_manager == true){templates = ['LoginEmployeeDetails','ManagerDashboard'];}
            else{ templates = ['LoginEmployeeDetails'];}
            _.each(templates, function(template) {
                self.$('.o_hr_dashboard').append(QWeb.render(template, {widget: self}));
            });
        }
        else{
                self.$('.o_hr_dashboard').append(QWeb.render('EmployeeWarning', {widget: self}));
            }
    },

    render_graphs: function(){console.log("RENDER_GRAPHS")
        var self = this;
        if (this.login_employee){
            self.render_branch_dept();
            // self.render_department_employee();
            self.render_job_graph();
            // self.update_join_resign_trends();
        }
    },

    on_reverse_breadcrumb: function() {console.log("ON_REVERSE_BREADCRUMB")
        var self = this;
        web_client.do_push_state({});
        this.update_cp();
        this.fetch_data().then(function() {
            self.$('.o_hr_dashboard').empty();
            self.render_dashboards();
            self.render_graphs();
        });
    },

    update_cp: function() {
        var self = this;
        console.log("UPDATE_CP")
//        this.update_control_panel(
//            {breadcrumbs: self.breadcrumbs}, {clear: true}
//        );
    },

    get_emp_image_url: function(employee){
        return window.location.origin + '/web/image?model=hr.employee&field=image_1920&id='+employee;
    },

    update_attendance: function () {
        var self = this;
        this._rpc({
            model: 'hr.employee',
            method: 'attendance_manual',
            args: [[self.login_employee.id], 'hr_attendance.hr_attendance_action_my_attendances'],
        })
        .then(function(result) {
            var attendance_state =self.login_employee.attendance_state;
            var message = ''
            var action_client = {
                type: "ir.actions.client",
                name: _t('Dashboard '),
                tag: 'hr_dashboard',
            };
            self.do_action(action_client, {clear_breadcrumbs: true});
            if (attendance_state == 'checked_in'){
                message = 'Checked Out'
            }
            else if (attendance_state == 'checked_out'){
                message = 'Checked In'
            }
            self.trigger_up('show_effect', {
                message: _t("Successfully " + message),
                type: 'rainbow_man'
            });
        });

    },

    hr_employee:function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Employee"),
            type: 'ir.actions.act_window',
            res_model: 'hr.employee',
            view_mode: 'kanban',
            context: {
                        'group_by' : 'branch_id',
                    },
            views: [[false, 'kanban'],[false, 'form']],
            target: 'current'
        }, options)
    },

    hr_department:function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Department"),
            type: 'ir.actions.act_window',
            res_model: 'hr.department',
            view_mode: 'form',
            views: [[false, 'list'],[false, 'form']],
            target: 'current'
        }, options)
    },

    hr_job_position:function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Job Position"),
            type: 'ir.actions.act_window',
            res_model: 'hr.job',
            view_mode: 'form',
            views: [[false, 'list'],[false, 'form']],
            target: 'current'
        }, options)
    },

    hr_res_branch:function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        var options = {
            on_reverse_breadcrumb: this.on_reverse_breadcrumb,
        };
        this.do_action({
            name: _t("Res Branch"),
            type: 'ir.actions.act_window',
            res_model: 'res.branch',
            view_mode: 'form',
            views: [[false, 'list'],[false, 'form']],
            target: 'current'
        }, options)
    },
    
   
    hr_contract: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        session.user_has_group('hr.group_hr_manager').then(function(has_group){
            if(has_group){
                var options = {
                    on_reverse_breadcrumb: self.on_reverse_breadcrumb,
                };
                self.do_action({
                    name: _t("Contracts"),
                    type: 'ir.actions.act_window',
                    res_model: 'hr.contract',
                    view_mode: 'kanban',
                    views: [[false, 'kanban'],[false, 'list'],[false, 'form']],
                    context: {
                        'group_by' : 'state',
                    },
                    target: 'current'
                }, options)
            }
        });

    },
    hr_resignation: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        session.user_has_group('hr.group_hr_manager').then(function(has_group){
            if(has_group){
                var options = {
                    on_reverse_breadcrumb: self.on_reverse_breadcrumb,
                };
                self.do_action({
                    name: _t("Resignation"),
                    type: 'ir.actions.act_window',
                    res_model: 'hr.resignation',
                    view_mode: 'tree,form',
                    views: [[false, 'list'],[false, 'form']],
                    domain: ['&',['resignation_type','=', 'resigned' ],['state', 'in', ['confirm', 'approved']]],
                    target: 'current'
                }, options)
            }
        });

    },
    hr_termination: function(e){
        var self = this;
        e.stopPropagation();
        e.preventDefault();
        session.user_has_group('hr.group_hr_manager').then(function(has_group){
            if(has_group){
                var options = {
                    on_reverse_breadcrumb: self.on_reverse_breadcrumb,
                };
                self.do_action({
                    name: _t("Resignation"),
                    type: 'ir.actions.act_window',
                    res_model: 'hr.resignation',
                    view_mode: 'tree,form',
                    views: [[false, 'list'],[false, 'form']],
                    domain: ['&',['resignation_type','=', 'fired' ],['state', 'in', ['confirm', 'approved']]],
                    target: 'current'
                }, options)
            }
        });

    },

    render_branch_dept:function(){
        var self = this;
        var w = 135;
        var h = 135;
        var r = h/2;
        var elem = this.$('.branch_graph');
       var colors = ['#ffa433', '#ffc25b', '#f8e54b', '#ff8762', '#5ebade', '#b298e1', '#70cac1', '#cf2030'];
        // var colors = ['#70cac1', '#659d4e', '#208cc2', '#4d6cb1', '#584999', '#8e559e', '#cf3650', '#f65337', '#fe7139',
        // '#ffa433', '#ffc25b', '#f8e54b'];
        var color = d3.scale.ordinal().range(colors);
        rpc.query({
            model: "res.branch",
            method: "get_branch_dept",
        }).then(function (data) {
            var segColor = {};
            var vis = d3.select(elem[0]).append("svg:svg").data([data]).attr("width", w).attr("height", h).append("svg:g").attr("transform", "translate(" + r + "," + r + ")");
            var pie = d3.layout.pie().value(function(d){return d.value;});
            var arc = d3.svg.arc().outerRadius(r);
            var arcs = vis.selectAll("g.slice").data(pie).enter().append("svg:g").attr("class", "slice");
            arcs.append("svg:path")
                .attr("fill", function(d, i){
                    return color(i);
                })
                .attr("d", function (d) {
                    return arc(d);
                });

            var legend = d3.select(elem[0]).append("table").attr('class','legend');

            // create one row per segment.
            var tr = legend.append("tbody").selectAll("tr").data(data).enter().append("tr");

            // create the first column for each segment.
            tr.append("td").append("svg").attr("width", '16').attr("height", '16').append("rect")
                .attr("width", '16').attr("height", '16')
                .attr("fill",function(d, i){ return color(i) });

            // create the second column for each segment.
            tr.append("td").text(function(d){ return d.label+" "+"Branch";});

            // create the third column for each segment.
            tr.append("td").attr("class",'legendFreq')
                .text(function(d){ return d.value +" "+"Dept";});
        });

    },
  
    // render_department_employee:function(){
    //     var self = this;
    //     var w = 135;
    //     var h = 135;
    //     var r = h/2;
    //     var elem = this.$('.emp_graph');
    //    var colors = ['#ff8762', '#5ebade', '#b298e1', '#70cac1', '#cf2030'];
    //     // var colors = ['#70cac1', '#659d4e', '#208cc2', '#4d6cb1', '#584999', '#8e559e', '#cf3650', '#f65337', '#fe7139',
    //     // '#ffa433', '#ffc25b', '#f8e54b'];
    //     var color = d3.scale.ordinal().range(colors);
    //     rpc.query({
    //         model: "hr.employee",
    //         method: "get_dept_employee",
    //     }).then(function (data) {
    //         var segColor = {};
    //         var vis = d3.select(elem[0]).append("svg:svg").data([data]).attr("width", w).attr("height", h).append("svg:g").attr("transform", "translate(" + r + "," + r + ")");
    //         var pie = d3.layout.pie().value(function(d){return d.value;});
    //         var arc = d3.svg.arc().outerRadius(r);
    //         var arcs = vis.selectAll("g.slice").data(pie).enter().append("svg:g").attr("class", "slice");
    //         arcs.append("svg:path")
    //             .attr("fill", function(d, i){
    //                 return color(i);
    //             })
    //             .attr("d", function (d) {
    //                 return arc(d);
    //             });

    //         var legend = d3.select(elem[0]).append("table").attr('class','legend');

    //         // create one row per segment.
    //         var tr = legend.append("tbody").selectAll("tr").data(data).enter().append("tr");

    //         // create the first column for each segment.
    //         tr.append("td").append("svg").attr("width", '16').attr("height", '16').append("rect")
    //             .attr("width", '16').attr("height", '16')
    //             .attr("fill",function(d, i){ return color(i) });

    //         // create the second column for each segment.
    //         tr.append("td").text(function(d){ return d.label;});

    //         // create the third column for each segment.
    //         tr.append("td").attr("class",'legendFreq')
    //             .text(function(d){ return d.value +" "+"Emp";});
    //     });

    // },
    // job position per departments
    render_job_graph:function(){
        var self = this;
        var w = 135;
        var h = 135;
        var r = h/2;
        var elem = this.$('.job_graph');
//        var colors = ['#ff8762', '#5ebade', '#b298e1', '#70cac1', '#cf2030'];
        var colors = ['#a1f344','#6cf8a6','#ff8762','#cf3650','#584999', '#8e559e', '#f65337', '#fe7139','#70cac1','#b298e1' , '#659d4e', '#208cc2', '#4d6cb1',
        '#ffa433', '#ffc25b', '#f8e54b'];
        var color = d3.scale.ordinal().range(colors);
        rpc.query({
            model: "hr.employee",
            method: "get_job_position",
        }).then(function (data) {
            var segColor = {};
            var vis = d3.select(elem[0]).append("svg:svg").data([data]).attr("width", w).attr("height", h).append("svg:g").attr("transform", "translate(" + r + "," + r + ")");
            var pie = d3.layout.pie().value(function(d){return d.value;});
            var arc = d3.svg.arc().outerRadius(r);
            var arcs = vis.selectAll("g.slice").data(pie).enter().append("svg:g").attr("class", "slice");
            arcs.append("svg:path")
                .attr("fill", function(d, i){
                    return color(i);
                })
                .attr("d", function (d) {
                    return arc(d);
                });

            var legend = d3.select(elem[0]).append("table").attr('class','legend');

            // create one row per segment.
            var tr = legend.append("tbody").selectAll("tr").data(data).enter().append("tr");

            // create the first column for each segment.
            tr.append("td").append("svg").attr("width", '16').attr("height", '16').append("rect")
                .attr("width", '16').attr("height", '16')
                .attr("fill",function(d, i){ return color(i) });

            // create the second column for each segment.
            tr.append("td").text(function(d){ return d.label+"  "+"Dept";});

            // create the third column for each segment.
            tr.append("td").attr("class",'legendFreq')
                .text(function(d){ return d.value+""+"JP";});
        });

    },

    // monthly  join and resign graph
    // update_join_resign_trends: function(){
    //     var elem = this.$('.join_resign_trend');
    //     var colors = ['#70cac1', '#659d4e', '#208cc2', '#4d6cb1', '#584999', '#8e559e', '#cf3650', '#f65337', '#fe7139',
    //     '#ffa433', '#ffc25b', '#f8e54b'];
    //     var color = d3.scale.ordinal().range(colors);
    //     rpc.query({
    //         model: "hr.employee",
    //         method: "join_resign_trends",
    //     }).then(function (data) {
    //         data.forEach(function(d) {
    //           d.values.forEach(function(d) {
    //             d.l_month = d.l_month;
    //             d.count = +d.count;
    //           });
    //         });
    //         var margin = {top: 30, right: 10, bottom: 30, left: 30},
    //             width = 400 - margin.left - margin.right,
    //             height = 250 - margin.top - margin.bottom;

    //         // Set the ranges
    //         var x = d3.scale.ordinal()
    //             .rangeRoundBands([0, width], 1);

    //         var y = d3.scale.linear()
    //             .range([height, 0]);

    //         // Define the axes
    //         var xAxis = d3.svg.axis().scale(x)
    //             .orient("bottom");

    //         var yAxis = d3.svg.axis().scale(y)
    //             .orient("left").ticks(5);

    //         x.domain(data[0].values.map(function(d) { return d.l_month; }));
    //         y.domain([0, d3.max(data[0].values, d => d.count)])

    //         var svg = d3.select(elem[0]).append("svg")
    //             .attr("width", width + margin.left + margin.right)
    //             .attr("height", height + margin.top + margin.bottom)
    //             .append("g")
    //             .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //         // Add the X Axis
    //         svg.append("g")
    //             .attr("class", "x axis")
    //             .attr("transform", "translate(0," + height + ")")
    //             .call(xAxis);

    //         // Add the Y Axis
    //         svg.append("g")
    //             .attr("class", "y axis")
    //             .call(yAxis);


    //         var line = d3.svg.line()
    //             .x(function(d) {return x(d.l_month); })
    //             .y(function(d) {return y(d.count); });

    //         let lines = svg.append('g')
    //           .attr('class', 'lines');

    //         lines.selectAll('.line-group')
    //             .data(data).enter()
    //             .append('g')
    //             .attr('class', 'line-group')
    //             .append('path')
    //             .attr('class', 'line')
    //             .attr('d', function(d) { return line(d.values); })
    //             .style('stroke', (d, i) => color(i));

    //         lines.selectAll("circle-group")
    //             .data(data).enter()
    //             .append("g")
    //             .selectAll("circle")
    //             .data(function(d) { return d.values;}).enter()
    //             .append("g")
    //             .attr("class", "circle")
    //             .append("circle")
    //             .attr("cx", function(d) { return x(d.l_month)})
    //             .attr("cy", function(d) { return y(d.count)})
    //             .attr("r", 3);

    //         var legend = d3.select(elem[0]).append("div").attr('class','legend');

    //         var tr = legend.selectAll("div").data(data).enter().append("div");

    //         tr.append("span").attr('class','legend_col').append("svg").attr("width", '16').attr("height", '16').append("rect")
    //             .attr("width", '16').attr("height", '16')
    //             .attr("fill",function(d, i){ return color(i) });

    //         tr.append("span").attr('class','legend_col').text(function(d){ return d.name;});
    //     });
    // },
   
});

core.action_registry.add('hr_dashboard', HrDashboard);

return HrDashboard;

});
