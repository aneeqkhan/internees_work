{
    'name': " HR Dashboard",
    'version': '13.0.1.0.2',
    'summary': """ HR Dashboard""",
    'description': """ HR Dashboard""",
    'category': 'Human Resources',
    'author': 'Romayyah',
    'depends': ['hr', 'hr_holidays', 'hr_employee_extension', 'hr_recruitment', 'hr_resignation', 'event',  'res_branch'],
    # 'external_dependencies': {
    #     'python': ['pandas'],
    # },
    'data': [
        'security/ir.model.access.csv',
        'report/broadfactor.xml',
        'views/dashboard_views.xml',
    ],
    'qweb': ["static/src/xml/hrms_dashboard.xml"],
    'images': ["static/description/banner.gif"],
    'license': "AGPL-3",
    'installable': True,
    'application': True,
}
