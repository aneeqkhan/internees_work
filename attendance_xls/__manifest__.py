{
'name': "Attendance Xls Report",
'summary': "Attendance Report",
'description': """
Track employee attendance
==============
 Attendance Excel Reports.
""",
'author': "Romayyah",
'category': 'Attendances',
'version': '13.0.1',
'depends': [ 'hr','report_xlsx'],
'data': [
   'views/attendance_view.xml',
   'reports/report.xml',
   'security/ir.model.access.csv'
    ],
'images': ['static/description/icon.png'],
}