from odoo import models, fields 

class AttendanceReport(models.TransientModel):
   _name = 'attendance.report'
   _description = 'Attendance Report'
  
   employee_ids = fields.Many2many(comodel_name='hr.employee',string='Employee')
 
   def action_print_xlsx_report(self):
      data={
         'model':'attendance.report',
         'form':self.read()[0],
      }
      return self.env.ref('attendance_xls.attendances_report_xlsx').report_action(self,data=data)
      