from datetime import datetime, date, timedelta
import base64
import io
from typing import NoReturn
from odoo import models

class AttendanceReportXlsx(models.AbstractModel):
    _name = 'report.attendance_xls.attendances_xls_report'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, attendance):
   
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        seial_number = workbook.add_format({
            "border_color": "#ABABAB",
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        bold1 = workbook.add_format({
            "align" : 'center',
            "valign" : 'vcenter',
            "bold": 1,
            "top": 2,
            "bottom": 2,
            "left": 1,
            "right": 1,
            "font_size" : '10',
         
        })
        bold = workbook.add_format({
            "align" : 'center',
            "valign" : 'vcenter',
            "bold": 1,
            "top": 1,
            "bottom": 1,
            "left": 1,
            "right": 1,
            "font_size" : '10',
         
        })
        worksheet = workbook.add_worksheet('Employee Attendance Report')
        worksheet.set_column('A:A',20)
        worksheet.set_column('B:E',25)
        
        row = 1
        col = 0

        worksheet.merge_range('C3:D4', 'Attendance Report',bold1)
      
        worksheet.write('A7', 'Sr.#',bold)
        worksheet.write('B7','Emp.Name',bold)
        worksheet.write('C7','Check In (Time)',bold)
        worksheet.write('D7','Check Out (Time)',bold)
        worksheet.write('E7','Work Hours',bold)

        domain = []
        if data['form']['employee_ids']:
            domain.append(('employee_id', 'in', data['form']['employee_ids']))
        employee_attendances = self.env['hr.attendance'].search(domain)

        row = 6
        s_no =  1

        for attendance in employee_attendances:
            row += 1
            worksheet.write(row, 0,s_no,seial_number)
            worksheet.write(row,col + 1,attendance.employee_id.name ,center)
            worksheet.write(row,col + 2, str(attendance.check_in if attendance.check_in else ' 0' ),center)
            worksheet.write(row,col + 3, str(attendance.check_out if attendance.check_out else ' 0' ),center)
            worksheet.write(row,col + 4,attendance.worked_hours if attendance.worked_hours else '0 ',center)
        
            # row += 1 
            s_no += 1    