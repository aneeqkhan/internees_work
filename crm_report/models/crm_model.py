from odoo import models, fields 
from datetime import datetime, date, timedelta
import logging

class CrmReport(models.TransientModel):
   _name = 'crm.report'
   _description = 'crm Report'

   partner_ids = fields.Many2many(comodel_name='res.partner' , string='Customer')
   date_deadlines = fields.Datetime(string ='Expected Closing')
   
   def action_print_xlsx_report(self):
      data={
         'model':'crm.report',
         'form':self.read()[0]
      }
      return self.env.ref('crm_report.crm_report_xlsx').report_action(self,data=data)