from datetime import datetime, date, timedelta
from datetime import datetime
import logging
import base64
import io
from typing import NoReturn
from odoo import models

class CrmReportXlsx(models.AbstractModel):
    _name = 'report.crm_report.crm_report_xls'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, crm_report):
        # print("hello from crm")
        
        #company
        company = self.env.company

        # user
        user = self.env.user
            
        #Design
        heading1 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#acbf69',
            'font_size': '10',
        })
        heading2 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#BADFE8',
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        seial_number = workbook.add_format({
            "border_color": "#ABABAB",
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        printed = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            # "bg_color": '#6FB3B8',
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'black',
            "bg_color": '#BADFE8',
        })
        worksheet = workbook.add_worksheet('CRM Excel Report')
        worksheet.set_column('A:M',20)
        worksheet.set_column('N:O',30)
        worksheet.set_column('P:Y',20)
      
        row = 1
        col = 0

        # Main heading of comapny data
        worksheet.merge_range('D1:G2', company.name, company_data)
        worksheet.merge_range('D3:G3', company.street ,company_data)
        worksheet.merge_range('D4:G4', company.email, company_data)
        worksheet.merge_range('D5:G5',company.phone ,company_data)

        # #heading of CRM details
        worksheet.merge_range('A9:L9','CRM ',heading1)
        # ,,,,,,,,,,,,,,,,,,,,,,,,,,
        worksheet.write('A10','No.',heading2)
        worksheet.write('B10', 'Opportunity', heading2)
        worksheet.write('C10', 'Expected Revenue', heading2)
        worksheet.write('D10','Probability',heading2)
        worksheet.write('E10','Customer',heading2)
        worksheet.write('F10','Email',heading2)
        worksheet.write('G10','Phone',heading2)
        worksheet.write('H10','Salesperson',heading2)
        worksheet.write('I10','Sales Team',heading2)
        worksheet.write('J10','Expected Closing',heading2)
        worksheet.write('K10','Priority',heading2)
        worksheet.write('L10','Tags',heading2)

        # Contact Information
        worksheet.merge_range('M9:S9','Contact Information',heading1)
        # ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
        worksheet.write('M10','Company Name', heading2)
        worksheet.write('N10','Address',heading2)
        worksheet.write('O10','Website',heading2)
        worksheet.write('P10','Language', heading2)
        worksheet.write('Q10','Contact Name',heading2)
        worksheet.write('R10','Job Position',heading2)
        worksheet.write('S10','Mobile',heading2)

        # Marketing
        worksheet.merge_range('T9:V9','Marketing',heading1)
        # ,,,,,,,,,,,,,,,,,,,,,,,,,,,
        worksheet.write('T10','Campaign',heading2)
        worksheet.write('U10','Medium',heading2)
        worksheet.write('V10','Source',heading2)

        # Misc
        worksheet.merge_range('W9:Y9','Misc',heading1)
        # ,,,,,,,,,,,,,,,,,,,,,
        worksheet.write('W10','Days to Assign',heading2)
        worksheet.write('X10','Days to Close',heading2)
        worksheet.write('Y10','Referred By',heading2)

        # domain
        domain = []
        if data['form']['partner_ids']:
            domain.append(('partner_id', 'in', data['form']['partner_ids']))
        if data['form']['date_deadlines']:
            domain.append(('date_deadline', '=', data['form']['date_deadlines']))         
        crm_report = self.env['crm.lead'].search(domain)    

        row = 9
        s_no =  1
        for crm in crm_report:
            row += 1
            worksheet.write(row, 0,s_no,seial_number)
            worksheet.write(row,col + 1,crm.name if crm.name else 0 ,center)
            worksheet.write(row,col + 2,crm.planned_revenue if crm.planned_revenue else 0 ,center)
            worksheet.write(row,col + 3,crm.probability if crm.probability else 0 ,center)
            worksheet.write(row,col + 4, crm.partner_id.name if crm.partner_id.name else 0 ,center)
            worksheet.write(row,col + 5, crm.email_from if crm.email_from else 0 ,center)
            worksheet.write(row,col + 6, crm.phone if crm.phone else 0 , center)
            worksheet.write(row,col + 7, crm.user_id.name if crm.user_id.name else 0 ,center)
            worksheet.write(row,col + 8, crm.team_id.name if crm.team_id.name else 0 ,center)
            worksheet.write(row,col + 9, str(crm.date_deadline if crm.date_deadline else 0),center)
            worksheet.write(row,col + 10, crm.priority if crm.priority else 0,center)
            worksheet.write(row,col + 11, crm.tag_ids if crm.tag_ids else 0,center)
            worksheet.write(row,col + 12, crm.partner_name if crm.partner_name else 0 ,center)
            worksheet.write(row,col + 13, crm.street if crm.street else 0 ,center)
            worksheet.write(row,col + 14, crm.website if crm.website else 0 ,center)
            worksheet.write(row,col + 15, crm.lang_id.name if crm.lang_id.name else 0 ,center)
            worksheet.write(row,col + 16, crm.contact_name if crm.contact_name else 0 ,center)
            worksheet.write(row,col + 17, crm.function if crm.function else 0, center)
            worksheet.write(row,col + 18, crm.mobile if crm.mobile else 0 ,center)
            worksheet.write(row,col + 19, crm.campaign_id.name if crm.campaign_id.name else 0 ,center)
            # logging.info(crm.campaign_id.name)
            worksheet.write(row,col + 20, crm.medium_id.name if crm.medium_id.name else 0 ,center)
            worksheet.write(row,col + 21, crm.source_id.name if crm.source_id.name else 0,center)
            worksheet.write(row,col + 22, crm.day_open if crm.day_open else 0,center)
            # logging.info(crm.day_open)
            worksheet.write(row,col + 23, crm.day_close if crm.day_close else 0 , center)
            worksheet.write(row,col + 24, crm.referred if crm.referred else 0 ,center)
            
            row +=1
            s_no += 1

        worksheet.write(row + 2, 0, "Printed by: ", printed)
        worksheet.write(row + 2, 1, user.name, printed) 