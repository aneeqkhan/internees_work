# -*- coding: utf-8 -*-
{
    'name': 'CRM Reports',
    'author' : 'Maria',
    'version': '1.1',
    'category': 'Human Resources',
    'summary': 'crm',
    'description': "",
    'depends': [
        'base',
        "crm",
        "report_xlsx",
        
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/crm_view.xml',
        'reports/report.xml',
        'reports/pdf_report.xml',
    
    ],   
}

