from odoo import models, fields ,api

class PlanningCommission(models.Model):
   _name = 'planning.commission'
   _description = 'Planning Commision'
   _inherit = ['mail.thread' , 'mail.activity.mixin']

   name = fields.Many2one('pc.config',string='Name of the Project', required=True )
   start_date = fields.Date(default=fields.Date.today)
   end_date = fields.Date(default=fields.Date.today)
   state = fields.Selection([('draft', 'Draft'), ('checked', 'Checked'), ('approved', 'Approved'), ('cancel', 'Cancel')] , default='draft')
