import string
from odoo import models, fields

class PcCategory(models.Model):
   _name = 'pc.category'
   name = fields.Char('Category Name')

class PcConfig(models.Model):
   _name = 'pc.config'

   name = fields.Char('Name')
   pc_category = fields.Many2one('pc.category', string='Category')

class PcSponsorship(models.Model):
   _name = 'pc.sponsorship'

   name = fields.Char('Sponsoring Name')
   street = fields.Char(string='Address')
   street = fields.Char()
   street2 = fields.Char()
   city = fields.Char()
   state_id = fields.Many2one('res.country.state',  string="Fed. State"  )
   zip = fields.Char()
   country_id = fields.Many2one('res.country',  string="Country")

class PcMainPower(models.Model):
   _name = 'pc.main.power'

   name = fields.Char(string='Name')
   mainpower_selection = fields.Selection([('employee', 'employee'),('user', 'user') ,('branch', 'branch') , ('department', 'department')], string="Selection")
   employee_ids = fields.Many2many('hr.employee', string="Employee")
   user_ids = fields.Many2many('res.users' , string='Users')
   branch_ids = fields.Many2many('res.branch' , string='Branch')
   department_ids = fields.Many2many('hr.department' , string='Department')