import string
from odoo import models, fields ,api

class PcThreeA(models.Model):
   _name = 'pc.three.a'
   _description = 'Planning Commision Three (a)'
   _inherit = ['mail.thread' , 'mail.activity.mixin']

   name = fields.Many2one('project.project',string='Name of the Project', required=True )
   approved_capital_cost= fields.Float('Approved Capital Cost')
   expenditure_last_financial_year = fields.One2many('expenditure.financial.year','expenditure_lines',string='Expenditure up to the end of last Financial Year')
   psdp_alloc_current_year = fields.One2many('psdp.allocation.current.year','psdp_lines',string='PSDP allocations for the Current year')
   annual_work_plan  = fields.One2many('annual.work.plan','annual_work_plan_lines', string="Annual work plan")
   quarterly_work_plan  = fields.One2many('quarterly.work.plan','quarterly_work_plan_lines', string="Quarterly work plan")
   cash_plan  = fields.One2many('cash.plan','cash_plan_lines', string="Cash Plan")

class ExpenditureFinancialYear(models.Model):
   _name = 'expenditure.financial.year'

   expenditure_lines = fields.Many2one('pc.three.a')
   actual = fields.Float(string='Actual')
   accrued = fields.Float(string='Accrued')
   total = fields.Float(string='Total' ,compute='_get_sum') 

   @api.depends('actual', 'accrued')
   def _get_sum(self):
      for rec in self:
         rec.update({
            'total': rec.actual+rec.accrued,
         })


class PsdpAllocationCurrentYear(models.Model):
   _name = 'psdp.allocation.current.year'
   _description = 'PSDP allocations for the Current year'

   psdp_lines = fields.Many2one('pc.three.a')
   psdp_alloc_total = fields.Float(string='Total')
   psdp_local = fields.Integer(string='Local')
   psdp_fec = fields.Integer(string='FEC')

class AnnualWorkPlan(models.Model):
   _name = 'annual.work.plan'
   _description = 'Annual Work Plan'

   annual_work_plan_lines = fields.Many2one('pc.three.a')
   annual_work_plan_items = fields.Many2one('product.product', string='Item')
   annual_work_plan_units = fields.Float(string='Unit')
   annual_work_plan_quantities = fields.Integer(string='Quantities')
   annual_work_plan_achivements = fields.Char(string='Achievements upto the end of last year')
   annual_work_plan_target = fields.Char(string='Target for current year')

class QuarterlyWorkPlan(models.Model):
   _name = 'quarterly.work.plan'
   _description = 'Quarterly work plan based on annual work plan'

   quarterly_work_plan_lines = fields.Many2one('pc.three.a')
   quarterly_work_plan_items = fields.Integer(string='Item')
   quarterly_work_plan_units = fields.Float(string='Unit')
   quarterly_work_plan_first_quarter = fields.Char(string='1st Quarter')
   quarterly_work_plan_sec_quarter = fields.Char(string='2nd Quarter')
   quarterly_work_plan_third_quarter = fields.Char(string='3rd Quarter')
   quarterly_work_plan_fourth_quarter = fields.Char(string='4th Quarter')

class CashPlan(models.Model):
   _name = 'cash.plan'
   _description = 'Cash Plan'

   cash_plan_lines = fields.Many2one('pc.three.a')
   cash_plan_first_quarter = fields.Char(string='1st Quarter')
   cash_plan_sec_quarter = fields.Char(string='2nd Quarter')
   cash_plan_third_quarter = fields.Char(string='3rd Quarter')
   cash_plan_fourth_quarter = fields.Char(string='4th Quarter')



