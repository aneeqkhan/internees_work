# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from odoo.exceptions import UserError, Warning, ValidationError
import logging
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
_logger = logging.getLogger(__name__)
from dateutil.relativedelta import relativedelta


class AttachmentInherit(models.Model):
    _inherit = "ir.attachment"

    budget_id = fields.Many2one('hr.budget',string="Budget")

# Hr Budget 
class HrBudgetLines(models.Model):
    
    _name = 'hr.budget.line'
    _order = "name"
    _description = "Budget lines"
    
    name=fields.Char('Description')
    job_id = fields.Many2one('hr.job', string='Job Position')
    department_id = fields.Many2one('hr.department',store=True,string='Department')
    contract_type_id = fields.Many2one('hr.contract.type', string='Contract Type')  
    throw_forward = fields.Float(string="Thrw.Fwd",store=True)
    creation = fields.Float(string="Creation")
    abolishment = fields.Float(string="Abolishment")
    budget_value = fields.Integer(string='Budget',store=True)
    actual_value = fields.Integer(string='Actual',readonly=True,compute='calculate_held')
    vacancies_value = fields.Integer(string='Vacancies',compute='compute_vacancies')
    budget_id = fields.Many2one('hr.budget', string='Budget')
    project = fields.Many2one('account.analytic.account', string="Project",related="budget_id.project_id",store=True)
    planned_amount = fields.Float(string='Planned Amount')
    actual_amount = fields.Float(string='Actual Amount',compute='_compute_actual_amount')
    variance_amount =fields.Float(compute="_compute_variance",string='Variance')

    state = fields.Selection([
        ('draft', 'Draft'),
        ('validate', 'Validated'),
        ('running','Running'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ], 'Status',related='budget_id.state')

    @api.onchange('job_id')
    def _onchange_job(self):
        if self.job_id:
            self.department_id = self.job_id.department_id.id


    @api.onchange('planned_amount')
    @api.depends('budget_id', 'planned_amount')
    def _compute_variance(self):
        for rec in self:
            rec.variance_amount = rec.planned_amount -  rec.actual_amount
    
    def calculate_held(self):
        current_date=fields.Date.today()
        for record in self:
            if record.budget_id.date_from and record.budget_id.date_to and record.budget_id.state not in ['done','cancel']:
                if current_date <= record.budget_id.date_to:
                    contract_ids = self.env['hr.contract'].sudo().search_count([
                    ('job_id','=',record.job_id.id), 
                    ('department_id','=',record.department_id.id), 
                    ('state', 'in',['open']),
                    ('type_id','=', record.contract_type_id.id ),
                   # ('date_start','<=',record.budget_id.date_to),
                   # ('date_start','>=',record.budget_id.date_from),
                    #('employee_id.project_id','=',record.project.id )
                ])
                if current_date > record.budget_id.date_to:
                    contract_ids = self.env['hr.contract'].sudo().search_count([
                    ('job_id','=',record.job_id.id), 
                    ('department_id','=',record.department_id.id), 
                    ('state', 'in',['open','close']),
                   # ('date_start','<=',record.budget_id.date_to),
                    #('date_start','>=',record.budget_id.date_from),
                    ('type_id','=', record.contract_type_id.id ),
                    #('employee_id.project_id','=',record.project.id )
                ])
                record.actual_value = contract_ids
            else:
                record.actual_value = 0

    
    def _compute_actual_amount(self):
        current_date=fields.Date.today()
        for record in self:
            if record.budget_id.date_from and record.budget_id.date_to and record.budget_id.state not in ['done','cancel']:
                payslip_ids = self.env['hr.payslip'].sudo().search([
                    ('job_id','=',record.job_id.id), 
                    ('dept_id','=',record.department_id.id), 
                    ('state', 'in',['done','verify']),
                    #('type_id','=', record.contract_type_id.id ),
                    ('date_from','<=',record.budget_id.date_to),
                    ('date_from','>=',record.budget_id.date_from),
                ])
                net_amount = 0
                for payslip in payslip_ids:
                    amount = 0
                    for line in payslip.line_ids:
                        if line.category_id.code == 'GROSS':
                            amount += line.total
                    net_amount += amount
                record.actual_amount = net_amount
            else:
                record.actual_amount = 0

    def view_payslip(self):
        current_date=fields.Date.today()
        for record in self:
            payslips = []
            if record.budget_id.date_from and record.budget_id.date_to and record.budget_id.state not in ['done','cancel']:
                payslip_ids = self.env['hr.payslip'].sudo().search([
                    ('job_id','=',record.job_id.id), 
                    ('dept_id','=',record.department_id.id), 
                    ('state', 'in',['done','verify']),
                    #('type_id','=', record.contract_type_id.id ),
                    ('date_from','<=',record.budget_id.date_to),
                    ('date_from','>=',record.budget_id.date_from),
                ])
                for payslip in payslip_ids:
                    payslips.append(payslip.id)
            
            return {
                'name': _('Payslip'),
                'view_type': 'form',
                'view_mode': 'kanban,tree,form',
                #'views': [(tree_view_id, 'tree'),(form_view_id, 'form')],
                'res_model': 'hr.payslip',
                'domain': [('id', 'in', payslips)],
                'type': 'ir.actions.act_window',
            }


    def view_contract(self):
        current_date=fields.Date.today()
        for record in self:
            contracts = []
            if record.budget_id.date_from and record.budget_id.date_to and record.budget_id.state not in ['done','cancel']:
                if current_date <= record.budget_id.date_to:
                    contract_ids = self.env['hr.contract'].sudo().search([
                    ('job_id','=',record.job_id.id), 
                    ('department_id','=',record.department_id.id), 
                    ('state', 'in',['open']),
                    ('type_id','=', record.contract_type_id.id ),
                    #('date_start','<=',record.budget_id.date_to),
                   # ('date_start','>=',record.budget_id.date_from),
                    #('employee_id.project_id','=',record.project.id )
                ])
                if current_date > record.budget_id.date_to:
                    contract_ids = self.env['hr.contract'].sudo().search([
                    ('job_id','=',record.job_id.id), 
                    ('department_id','=',record.department_id.id), 
                    ('state', 'in',['open','close']),
                   # ('date_start','<=',record.budget_id.date_to),
                   # ('date_start','>=',record.budget_id.date_from),
                    ('type_id','=', record.contract_type_id.id ),
                    #('employee_id.project_id','=',record.project.id )
                ])
                for c in contract_ids:
                    contracts.append(c.id)
                return {
                    'name': _('Contracts'),
                    'view_type': 'form',
                    'view_mode': 'kanban,tree,form',
                    #'views': [(tree_view_id, 'tree'),(form_view_id, 'form')],
                    'res_model': 'hr.contract',
                    'domain': [('id', 'in', contracts)],
                    'type': 'ir.actions.act_window',
                }

    @api.onchange('creation','abolishment')
    def _onchange_creation(self):
        self.budget_value = (self.throw_forward + self.creation) - self.abolishment
    

    @api.onchange('budget_value')
    def check_budget_value(self):
        for record in self:
            if record.budget_value < 0:
                raise ValidationError("Budget value must be positive integer")

    
    def compute_vacancies(self):
        for record in self:
            record.vacancies_value = record.creation - record.actual_value


class HrJobLines(models.Model):
    _name = 'hr.budget.job.line'

    name = fields.Many2one('hr.job', string='Job Position')
    department_id = fields.Many2one('hr.department', string='Department')
    anayltic_account_id= fields.Many2one('account.analytic.account',string= 'Analytic Account')

class ActionMenu(models.TransientModel):
    _name = 'budget.action.menu'

    def action_compute_sheet(self):
        active_ids = self._context.get('active_ids', []) or []
        self.env['hr.budget'].browse(active_ids).action_budget_compute_sheet()


class HrBudget(models.Model):
    _name = 'hr.budget'
    _description = 'HR Budget'
    _inherit = ['mail.thread']

    name = fields.Char('Budget Name')


    def _get_default_date_from(self):
        Current_date = datetime.today()
        start = Current_date.strftime('%Y-07-01')
        if start:
            return start
        else:
            None

    def _get_default_date_end(self):
        Current_date = datetime.today()
        n_date = Current_date + relativedelta(years=1)
        end_date = n_date.strftime('%Y-06-30')
        if end_date:
            return end_date
        else:
            None

    date_from = fields.Date(string='Start Date',track_visibility='onchange',default=_get_default_date_from )
    date_to = fields.Date(string='End Date',track_visibility='onchange',default=_get_default_date_end)
    line_ids = fields.One2many('hr.budget.line', 'budget_id', string='Budget Lines', track_visibility='always')
    project_id = fields.Many2one('account.analytic.account', 'Project',track_visibility='onchange')
    donor_id = fields.Many2one('res.partner',string="Donor")
    previous_budget = fields.Many2one('hr.budget',string="Previous Budget",domain="[('project_id', '=', project_id)]")
    ref = fields.Text(string='Reference')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('validate', 'Validated'),
        ('running','Running'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')
    revisions = fields.One2many('hr.budget.revisions', 'name', string='Revisions History')
    group_id = fields.Many2one('budget.group',string="Parent Budget")
    parent_id = fields.Many2one(comodel_name='hr.budget', string='Parent Budget', index=True, ondelete='cascade')
    child_ids = fields.One2many(comodel_name='hr.budget', inverse_name='parent_id', string='Budgets')
    
    attachement_ids = fields.One2many('ir.attachment', 'budget_id', string="Attachments")


    @api.model
    def create(self, vals):
        result = super(HrBudget,self).create(vals)
        bgt_name = 'HR Budget - '
        if result.project_id:
            bgt_name += result.project_id.name +  ' '

        if result.group_id:
            bgt_name += '- ' + result.group_id.name +  ' '
        result.name = bgt_name
        # if result.date_from and result.date_to:
        #     bgt_name += '('+datetime.strptime(fields.Datetime.to_string(result.date_from), DF).strftime('%B')+'-'+datetime.strptime(fields.Datetime.to_string(result.date_from), DF).strftime('%Y')+' to '
        #     bgt_name += datetime.strptime(fields.Datetime.to_string(result.date_to), DF).strftime('%B')+'-'+datetime.strptime(fields.Datetime.to_string(result.date_to), DF).strftime('%Y')+')'
        #     result.name = bgt_name
        return result
    
    
    def write(self, vals):
        result = super(HrBudget,self).write(vals)
        if 'project_id' in vals or 'date_from' in vals or 'date_to' in vals:
            for record in self:
                bgt_name = 'HR Budget - '
                if record.project_id:
                    bgt_name += record.project_id.name +  ' '
                record.name = bgt_name
                # if record.date_from and record.date_to:
                #     bgt_name += '('+datetime.strptime(fields.Datetime.to_string(record.date_from), DF).strftime('%B')+'-'+datetime.strptime(fields.Datetime.to_string(record.date_from), DF).strftime('%Y')+' to '
                #     bgt_name += datetime.strptime(fields.Datetime.to_string(record.date_to), DF).strftime('%B')+'-'+datetime.strptime(fields.Datetime.to_string(record.date_to), DF).strftime('%Y')+')'
                #     record.name = bgt_name
    
    @api.onchange('project_id','date_from','date_to')
    def check_existing_budgets(self):
        budgets = self.env['hr.budget'].search([('project_id','=',self.project_id.id), ('group_id','=',self.group_id.id) ])
        for record in budgets:        
            if self.date_from and self.date_to:
                if record.date_from == self.date_from and record.date_to == self.date_to:
                    raise Warning(_("HR Budget for (%s - %s) already exist-->(%s)") % (str(record.date_from),str(record.date_to),record.name))
    
       
    #  Function for loading budget lines with respect to project            
    def load_Budget_Lines(self):
        for rec in self:
            for line in rec.project_id.job_ids:       
                if not (line.id in rec.line_ids.mapped('job_id').ids):
                    rec.line_ids = [[0,0,{
                        'name': line.name.name,  
                        'job_id': line.id,
                        'budget_value': 0,
                        'actual_value': 0,
                        'vacancies_value': 0,
                        'budget_id': rec.id,
                        }]]
        if self.previous_budget:
            for prev_line in self.previous_budget.line_ids:
                for curr_line in self.line_ids:
                    if prev_line.job_id.id == curr_line.job_id.id:
                        curr_line.write({
                            'throw_forward': prev_line.budget_value
                        })
                
        
#   Functions for changing states using buttons     
    
    def action_budget_draft(self):
        self.write({'state': 'draft'})
   
    def action_budget_validate(self):
        self.write({'state': 'validate'})
    
    
    def action_budget_confirm(self):
        self.write({'state': 'running'})
    
   
    def action_budget_done(self):
        self.write({'state': 'done'})
        self.action_budget_compute_sheet()
        
    
    def action_budget_cancel(self):
        self.write({'state': 'cancel'})   
    
    def action_budget_compute_sheet(self):
        current_date=fields.Date.today()
        for line in self:
             for record in line.line_ids:
                 if record.budget_id.date_from and record.budget_id.date_to:
                     if current_date <= record.budget_id.date_to:
                            contract_ids = self.env['hr.contract'].sudo().search_count([
                            ('job_id','=',record.job_id.id), 
                            ('department_id','=',record.department_id.id), 
                            ('state', 'in',['open']),
                            ('type_id','=', record.contract_type_id.id ),
                            ('date_end','<=',record.budget_id.date_to),
                            ('date_end','>=',record.budget_id.date_from),
                            #('employee_id.project_id','=',record.project.id )
                     ])
                     if current_date > record.budget_id.date_to:
                            contract_ids = self.env['hr.contract'].sudo().search_count([
                            ('job_id','=',record.job_id.id), 
                            ('department_id','=',record.department_id.id), 
                            ('state', 'in',['open','close']),
                            ('date_end','<=',record.budget_id.date_to),
                            ('date_end','>=',record.budget_id.date_from),
                            ('type_id','=', record.contract_type_id.id ),
                            #('employee_id.project_id','=',record.project.id )
                     ])
                     record.actual_value = contract_ids
        return         
        
class ProjectAnalyticInherit(models.Model):
     
    _inherit = 'account.analytic.account'

    job_ids = fields.One2many('hr.budget.job.line',inverse_name="anayltic_account_id",string="Jobs")
    aa_type = fields.Char("AA_Type")
    # address = fields.Char("Address")

    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char()
    city = fields.Char()
    state_id = fields.Many2one('res.country.state',  string="Fed. State")
    country_id = fields.Many2one('res.country',  string="Country")


class BudgetGroup(models.Model):
    
    _name = "budget.group"
    
    name = fields.Char(required=True)
    code_prefix = fields.Char("Code Prefix")
    # through_forwarding = fields.Boolean('Enable Through-Forwarding')
    # variance_forwarding = fields.Boolean('Enable Variance Forwarding')


class BudgetRevisions(models.Model):
    
    _name = "hr.budget.revisions"

    name = fields.Many2one('hr.budget', 'Budget')
    analytic_account = fields.Many2one('account.analytic.account', 'Project')
    previous_budget_value = fields.Float('Before Revision')
    after_revision_value = fields.Float(string='After Revision')
    line_reference = fields.Many2one('hr.budget.line', 'Line Reference')
    job_id = fields.Many2one('hr.job', 'Job Position')