from odoo import models, fields 
import logging

class BudgetReport(models.TransientModel):
   _name = 'budget.report'
   _description = 'Budget Report'
  
   group_ids = fields.Many2many(comodel_name='budget.group',string='Budget Type')
   project_ids = fields.Many2many(comodel_name='account.analytic.account',string='Analytic Account')
    
   def action_print_xlsx_report(self):
        data={
            'model':'budget.report',
            'form':self.read()[0],
        }
        return self.env.ref('custom_budget_hr.budget_report_xlsx').report_action(self,data=data)

