{
    'name': "Custom Budget HR",
    'author': "Aneeq",
    'category': 'Accounting and Finance',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'hr',
        'hr_contract_extension',
        'report_xlsx',
    ],
    # always loaded
    'data': [
        'security/custom_budget_groups.xml',
        'security/ir.model.access.csv',
        'wizard/budget_revesion_view.xml',
        'reports/budget_pdf.xml',
        'reports/report.xml',
        'views/views.xml',
        'views/budget_report.xml',
        
        # 'views/templates.xml',
        #'custom_reports/budget_by_project.xml',
        #'custom_reports/budget_var_report.xml',
        #'custom_reports/budget_by_job.xml',  
    ],
}