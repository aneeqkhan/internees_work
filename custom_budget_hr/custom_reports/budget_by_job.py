# -*- coding: utf-8 -*-

import time
from datetime import datetime
from datetime import date,timedelta
from flectra import api, models, _, fields
from flectra.tools.misc import formatLang
from flectra.exceptions import UserError,ValidationError
import logging
from itertools import groupby
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT as DF,DEFAULT_SERVER_DATETIME_FORMAT as DTF
from dateutil import rrule

class HourlyAttendance(models.AbstractModel):
    _name = "hourly.attendance.report"
    _description = "Hourly Attendnace Report"
    _inherit = 'account.report'

    filter_date = {'date_from': '', 'date_to': '', 'filter': 'this_month'}
    filter_show_daily_hours = False
    filter_show_weekly_hours = False
    filter_show_monthly_hours = False
    filter_show_yearly_hours = False
    filter_branch = True
    filter_department = True
    filter_project = True
    
    
    
    def get_columns_name(self, options):
        columns = []
        columns.append({'name': _('Employee')})
        start_date = datetime.strptime(options['date']['date_from'],DF)
        end_date = datetime.strptime(options['date']['date_to'],DF)
        
        if (options.get('show_daily_hours')):
            for dt in rrule.rrule(rrule.DAILY, dtstart=start_date, until=end_date):
                columns.append({'name': _(dt.strftime("%d/%b/%y")),'class':'number'})
        if (options.get('show_weekly_hours')):
            for dt in rrule.rrule(rrule.WEEKLY, dtstart=start_date, until=end_date):
                columns.append({'name': _(dt.strftime("Week(%U)")),'class':'number'})
        if (options.get('show_monthly_hours')):
            for dt in rrule.rrule(rrule.MONTHLY, dtstart=start_date, until=end_date):
                columns.append({'name': _(dt.strftime("%b/%y")),'class':'number'})
        if (options.get('show_yearly_hours')):
            for dt in rrule.rrule(rrule.YEARLY, dtstart=start_date, until=end_date):
                columns.append({'name': _(dt.strftime("%Y")),'class':'number'})
        return columns
    
    @api.model
    def get_lines(self, options, line_id=None):
        lines = []
        all_attendances = self.env['hr.attendance'].search([('check_in','>=',options['date']['date_from']),('check_out','<=',options['date']['date_to'])],order='employee_id asc')
        for employee,attendances in groupby(all_attendances,lambda att: att.employee_id.id):
            emp_id = self.env['hr.employee'].browse(employee)
            attend_ids = self.env['hr.attendance'].browse([attend.id for attend in attendances])
            columns = []
            start_date = datetime.strptime(options['date']['date_from'],DF)
            end_date = datetime.strptime(options['date']['date_to'],DF)
            if (options.get('show_daily_hours')): 
                for dt in rrule.rrule(rrule.DAILY, dtstart=start_date, until=end_date):
                    total_hours = 0
                    for attend in attend_ids:
                        if datetime.strptime(attend.check_in,DTF).date() >= dt.date() and datetime.strptime(attend.check_out,DTF).date() <= dt.date():
                            total_hours += attend.current_hours
                    columns += [{'name': '{0:02.0f}:{1:02.0f}'.format(*divmod(total_hours * 60, 60))}]
            if (options.get('show_weekly_hours')): 
                for dt in rrule.rrule(rrule.WEEKLY, dtstart=start_date, until=end_date):
                    total_hours = 0
                    for attend in attend_ids:
                        if int(datetime.strptime(attend.check_in,DTF).strftime("%U")) >= int(dt.strftime("%U")) and int(datetime.strptime(attend.check_out,DTF).strftime("%U")) <= int(dt.strftime("%U")):
                            total_hours += attend.current_hours
                    columns += [{'name': '{0:02.0f}:{1:02.0f}'.format(*divmod(total_hours * 60, 60))}]
            if (options.get('show_monthly_hours')): 
                for dt in rrule.rrule(rrule.MONTHLY, dtstart=start_date, until=end_date):
                    total_hours = 0
                    for attend in attend_ids:
                        if int(datetime.strptime(attend.check_in,DTF).strftime("%m")) >= int(dt.strftime("%m")) and int(datetime.strptime(attend.check_out,DTF).strftime("%m")) <= int(dt.strftime("%m")):
                            total_hours += attend.current_hours
                    columns += [{'name': '{0:02.0f}:{1:02.0f}'.format(*divmod(total_hours * 60, 60))}]
            if (options.get('show_yearly_hours')): 
                for dt in rrule.rrule(rrule.YEARLY, dtstart=start_date, until=end_date):
                    total_hours = 0
                    for attend in attend_ids:
                        if int(datetime.strptime(attend.check_in,DTF).strftime("%y")) >= int(dt.strftime("%y")) and int(datetime.strptime(attend.check_out,DTF).strftime("%y")) <= int(dt.strftime("%y")):
                            total_hours += attend.current_hours
                    columns += [{'name': '{0:02.0f}:{1:02.0f}'.format(*divmod(total_hours * 60, 60))}]
            
            lines.append({
                'id': emp_id.name + str(emp_id),
                'name': emp_id.name,
                'level': 2,
                # 'class':'total',
                # 'unfoldable': True,
                'unfolded': line_id == None and True or False,
                # 'style': 'background:#ebedf0',
                'columns': columns,
                })
        
        return lines

    def get_report_name(self):
        return _('Hourly Attendance Report')
    
    def get_templates(self):
        templates = super(HourlyAttendance, self).get_templates()
        templates['search_template'] = 'hr_attendance_enhancement.search_template_hourly_attendance_report'
        return templates