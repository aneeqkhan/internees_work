# -*- coding: utf-8 -*-

import time
from datetime import datetime
from datetime import date
from flectra import api, models, _, fields
from flectra.tools.misc import formatLang
from flectra.exceptions import UserError,ValidationError
import logging

class BudgetLines(models.Model):
    _inherit = 'hr.budget.line'

    bgt_date_from = fields.Date('Start Date', related="budget_id.date_from", store=True)
    bgt_date_to = fields.Date('End Date', related="budget_id.date_to", store=True)
    project = fields.Many2one('account.analytic.account', string="Project",related="budget_id.project_id",store=True)
    grade = fields.Many2one("grade.type","Grade", related="job_id.grade",store=True)
   
class ProjectBudget(models.AbstractModel):
    _name = "project.hr.budgets.report"
    _description = "Analytic Account Budget Report"
    _inherit = 'account.report'

    filter_date = {'date_from': '', 'date_to': '','filter': 'this_year'}
    filter_analytic = True
    filter_comparison = {'date_from': '', 'date_to': '', 'filter': 'no_comparison', 'number_period': 1}
    
    filter_show_thw_fwd = True
    filter_show_creation = True
    filter_show_abolishment = True

    filter_show_budget_amount = True
    filter_show_actual_amount = True
    filter_show_variance_amount = True
    
    filter_multiple1 = True      # for Department
    filter_multiple1_ids = []

    filter_multiple2 = True      # for contract type
    filter_multiple2_ids = []

    filter_multiple3 = True      # for job position
    filter_multiple3_ids = []

    filter_multiple5 = True      # for Grade
    filter_multiple5_ids = None



    def get_columns_name(self, options):
        columns = []
        columns.append({'name': _('')})
        if options['date'] and options.get('date'):
        
            if (options.get('show_thw_fwd')) and (options.get('show_creation')) and (options.get('show_abolishment')) and  (options.get('show_budget_amount') and options.get('show_actual_amount') and options.get('show_variance_amount')): # and options.get('show_actual_amount') and options.get('show_variance_amount')):
                columns.append({'name': ' ', 'class': 'number'})
                columns.append({'name': ' ', 'class': 'number'})
                columns.append({'name': ' ', 'class': 'number'})
                columns.append({'name': options['date'].get('string')})
                columns.append({'name': ' ', 'class': 'number'})
                columns.append({'name': ' ', 'class': 'number'})
            else:
                if (options.get('show_thw_fwd')):
                    columns.append({'name': options['date'].get('string')})
                if (options.get('show_creation')):
                    columns.append({'name': options['date'].get('string')})
                if (options.get('show_abolishment')):
                    columns.append({'name': options['date'].get('string')})

                if (options.get('show_budget_amount')): # and options.get('show_actual_amount') and options.get('show_variance_amount')):
                    columns.append({'name': options['date'].get('string')})
                if (options.get('show_actual_amount')):
                    columns.append({'name': options['date'].get('string')})
                if (options.get('show_variance_amount')):
                    columns.append({'name': options['date'].get('string')})

        if options['comparison'] and options.get('comparison') and options['comparison'].get('periods'):
            for period in options['comparison']['periods']:
                if (options.get('show_thw_fwd')) and (options.get('show_creation')) and (options.get('show_abolishment')) and (options.get('show_budget_amount') and options.get('show_actual_amount') and options.get('show_variance_amount')): # and options.get('show_actual_amount') and options.get('show_variance_amount')):
                    columns += [{'name': ' ', 'class': 'number'}]
                    columns += [{'name': ' ', 'class': 'number'}]
                    columns += [{'name': ' ', 'class': 'number'}]
                    columns += [{'name': period.get('string')}]
                    columns += [{'name': ' ', 'class': 'number'}]
                    columns += [{'name': ' ', 'class': 'number'}]
                else:
                    if (options.get('show_thw_fwd')):
                        columns += [{'name': period.get('string')}]
                    if (options.get('show_creation')):
                        columns += [{'name': period.get('string')}]
                    if (options.get('show_abolishment')):
                        columns += [{'name': period.get('string')}]

                    if (options.get('show_budget_amount')):
                        columns += [{'name': period.get('string')}]
                    if (options.get('show_actual_amount')):
                        columns += [{'name': period.get('string')}]
                    if (options.get('show_variance_amount')):
                        columns += [{'name': period.get('string')}]
            
        return columns
    
    @api.multi
    def get_report_informations(self, options):
        options = self.get_options(options)
        options = self.apply_date_filter(options)
        options = self.apply_cmp_filter(options)
        searchview_dict = {'options': options, 'context': self.env.context}
        searchview_dict['multiple1_ids'] = [(t.id, t.name) for t in self.env['hr.department'].search([])] or False
        searchview_dict['multiple2_ids'] = [(t.id, t.name) for t in self.env['hr.contract.type'].search([])] or False
        searchview_dict['multiple3_ids'] = [(t.id, t.name) for t in self.env['hr.job'].search([])] or False
        searchview_dict['multiple5_ids'] = [(t.id, t.name) for t in self.env['grade.type'].search([])] or False
        report_manager = self.get_report_manager(options)
        info = {'options': options,
                'context': self.env.context,
                'report_manager_id': report_manager.id,
                'buttons': self.get_reports_buttons(),
                'main_html': self.get_html(options),
                'searchview_html': self.env['ir.ui.view'].render_template(self.get_templates().get('search_template', 'custom_budget_hr.search_template_budget_var'), values=searchview_dict),
                }
        return info

    @api.model
    def get_lines(self, options, line_id=None):
        lines = []

        first_row_col = []
        if (options.get('show_thw_fwd')):
            first_row_col += [{'name': 'Throw-Forward', 'class': 'number'}]
        if (options.get('show_creation')):
            first_row_col += [{'name': 'Creation', 'class': 'number'}]
        if (options.get('show_abolishment')):
            first_row_col += [{'name': 'Abolishment', 'class': 'number'}]

        if (options.get('show_budget_amount')):
            first_row_col += [{'name': 'Budget', 'class': 'number'}]
        if (options.get('show_actual_amount')):
            first_row_col += [{'name': 'Held', 'class': 'number'}]
        if (options.get('show_variance_amount')):
            first_row_col += [{'name': 'Vacant', 'class': 'number'}]
        if options.get('comparison') and options['comparison'].get('periods'):
                for period in options['comparison']['periods']:
                    if (options.get('show_thw_fwd')):
                        first_row_col += [{'name': 'Throw-Forward', 'class': 'number'}]
                    if (options.get('show_creation')):
                        first_row_col += [{'name': 'Creation', 'class': 'number'}]
                    if (options.get('show_abolishment')):
                        first_row_col += [{'name': 'Abolishment', 'class': 'number'}]

                    if (options.get('show_budget_amount')):
                        first_row_col += [{'name': 'Budget', 'class': 'number'}]
                    if (options.get('show_actual_amount')):
                        first_row_col.append({'name': 'Held', 'class': 'number'})
                    if (options.get('show_variance_amount')):
                        first_row_col.append({'name': 'Vacant', 'class': 'number'})
        lines.append({
                    'id': 0,
                    'name': ' ',
                    'level': 1,
                    'unfoldable': False,
                    'columns':first_row_col,
                })

        project_query = """
            SELECT account_analytic_account.name as name,account_analytic_account.id as id
            FROM account_analytic_account
        """

        p = '('
        if options['analytic_accounts']:
            for x in options['analytic_accounts'][:-1]:
                p+=x+','
            p+=options['analytic_accounts'][-1]+')'
            var = "WHERE id IN " + p +'ORDER BY id ASC'
            project_query += var

        self.env.cr.execute(project_query)
        projects = self.env.cr.dictfetchall()    
        for project in projects:    
            proj_columns = []
            if (options.get('show_thw_fwd')):
                proj_columns += [{'name': ' ', 'class': 'number'}]
            if (options.get('show_creation')):
                proj_columns += [{'name': ' ', 'class': 'number'}]
            if (options.get('show_abolishment')):
                proj_columns += [{'name': ' ', 'class': 'number'}]

            if (options.get('show_budget_amount')):
                proj_columns += [{'name': ' ', 'class': 'number'}]
            if (options.get('show_actual_amount')):
                proj_columns.append({'name': ' ', 'class': 'number'})
            if (options.get('show_variance_amount')):
                proj_columns.append({'name': ' ', 'class': 'number'})
            if options.get('comparison') and options['comparison'].get('periods'):
                for period in options['comparison']['periods']:
                    if (options.get('show_thw_fwd')):
                        proj_columns += [{'name': ' ', 'class': 'number'}]
                    if (options.get('show_creation')):
                        proj_columns += [{'name': ' ', 'class': 'number'}]
                    if (options.get('show_abolishment')):
                        proj_columns += [{'name': ' ', 'class': 'number'}]

                    if (options.get('show_budget_amount')):
                        proj_columns += [{'name': ' ', 'class': 'number'}]
                    if (options.get('show_actual_amount')):
                        proj_columns.append({'name': ' ', 'class': 'number'})
                    if (options.get('show_variance_amount')):
                        proj_columns.append({'name': ' ', 'class': 'number'})
            
            lines.append({
                    'id': project.get('id'),
                    'name': project.get('name'),
                    'level': 2,
                    'unfoldable': True,
                    'unfolded': True,#line_id == proj.get('id') and True or False,
                    'style': 'background:#ebedf0',
                    'columns':proj_columns,
                })
            
         
            # Budget Related Work .....................................................................
            bl_query = """
                    SELECT
                        SUM(hr_budget_line.budget_value) AS budget_value,
                        SUM(hr_budget_line.throw_forward) AS throw_forward,
                        SUM(hr_budget_line.creation) AS creation,
                        SUM(hr_budget_line.abolishment) AS abolishment,
                        hr_budget_line.project as project,
                        hr_budget_line.job_id AS job_id,
                        hr_job.name AS job_name
                    FROM hr_budget_line
                    Full JOIN hr_job on hr_budget_line.job_id = hr_job.id
                    Full JOIN hr_department on hr_budget_line.department_id = hr_department.id
                    Full JOIN hr_contract_type on hr_budget_line.contract_type_id = hr_contract_type.id
                    Full JOIN grade_type on hr_budget_line.grade = grade_type.id
                    WHERE hr_budget_line.project = """ + str(project.get('id'))  
            
            if options['date']:
                bl_query += " AND Date(hr_budget_line.bgt_date_from) >= %s "
                bl_query += " AND Date(hr_budget_line.bgt_date_to) <= %s "
            
           
            q = '('
            if options['multiple3_ids']:
                for x in options['multiple3_ids'][:-1]:
                    q+=x+','
                q+=options['multiple3_ids'][-1]+')'
                var2 = "AND hr_job.id IN " + q
                bl_query += var2
            
            r = '('
            if options['multiple1_ids']:
                for x in options['multiple1_ids'][:-1]:
                    r+=x+','
                r+=options['multiple1_ids'][-1]+')'
                var2 = "AND hr_department.id IN " + r
                bl_query += var2
            
            s = '('
            if options['multiple5_ids']:
                for x in options['multiple5_ids'][:-1]:
                    s+=x+','
                s+=options['multiple5_ids'][-1]+')'
                var2 = "AND grade_type.id IN " + s
                bl_query += var2

            bl_query += " GROUP BY job_name,job_id, project "
            
            self.env.cr.execute(bl_query,(options['date']['date_from'],options['date']['date_to']) )
            budget_lines = self.env.cr.dictfetchall()
            
        
            for line in budget_lines:
                line_columns = []

                if (options.get('show_thw_fwd')):
                    line_columns += [{'name': line.get('throw_forward') , 'class': 'number'}]
                if (options.get('show_creation')):
                    line_columns += [{'name': line.get('creation'), 'class': 'number'}]
                if (options.get('show_abolishment')):
                    line_columns += [{'name': line.get('abolishment'), 'class': 'number'}]

                if (options.get('show_budget_amount')):
                    line_columns += [{'name': '{:,.2f}'.format(line.get('budget_value')), 'class': 'number'}]
                actual_value = self.env['hr.contract'].search_count([('job_id','=',line.get('job_id')), ('state','=','open'),('employee_id.project_id','=',project.get('id') )])
                if (options.get('show_actual_amount')):

                    line_columns.append({'name': '{:,.2f}'.format(actual_value), 'class': 'number'})
                if (options.get('show_variance_amount')):
                    line_columns.append({'name': '{:,.2f}'.format(line.get('budget_value') - actual_value ),'class': 'number'})
                
                if options.get('comparison') and options['comparison'].get('periods'):
                    for period in options['comparison']['periods']:
                        bgt_line = self.env['hr.budget.line'].search([('bgt_date_to', '=',period['date_to']),('job_id','=',self.env['hr.job'].search([('id','=',line.get('job_id'))],limit=1).id)])
                        
                        if (options.get('show_thw_fwd')):
                            line_columns += [ {'name': bgt_line.throw_forward, 'class': 'number'}]
                        if (options.get('show_creation')):
                            line_columns += [{'name': bgt_line.creation, 'class': 'number'}]
                        if (options.get('show_abolishment')):
                            line_columns += [{'name': bgt_line.abolishment, 'class': 'number'}]


                        
                        if (options.get('show_budget_amount')):
                            line_columns += [{'name': '{:,.2f}'.format(bgt_line.budget_value), 'class': 'number'}]
                        actual_value1 = self.env['hr.contract'].search_count([('job_id','=', bgt_line.job_id.id ), ('state','=','open'),('employee_id.project_id','=',project.get('id') )])
                        if (options.get('show_actual_amount')):
                            
                            line_columns.append({'name': '{:,.2f}'.format(actual_value1), 'class': 'number'})
                        if (options.get('show_variance_amount')):
                            line_columns.append({'name': '{:,.2f}'.format(bgt_line.budget_value - actual_value1)})
                lines.append({
                    'id': line.get('job_name'),
                    'name':  line.get('job_name'),
                    'level': 3,
                    'unfoldable': False,
                    'columns':line_columns,
                })

            #Total Columns-----------------------------------------------------
            total_column = []
            total_query = """
                    SELECT
                        SUM(hr_budget_line.budget_value) AS budget_value,
                        SUM(hr_budget_line.throw_forward) AS throw_forward,
                        SUM(hr_budget_line.creation) AS creation,
                        SUM(hr_budget_line.abolishment) AS abolishment,
                        hr_budget_line.project AS project        
                    FROM
                        hr_budget_line
                    WHERE project = %s """
            if options['date']:
                total_query += " AND Date(hr_budget_line.bgt_date_from) >= %s "
                total_query += " AND Date(hr_budget_line.bgt_date_to) <= %s "
            total_query += " GROUP BY project"
            self.env.cr.execute(total_query,(project.get('id'),options['date']['date_from'],options['date']['date_to']))    
            re = self.env.cr.fetchone()
            if re:
                budget_amount = re[0] or 0.0
                th_amount = re[1] or 0.0
                creation_amount = re[2] or 0.0
                abolishment_amount = re[3] or 0.0
            else:
                budget_amount = 0
                th_amount = 0
                creation_amount =0
                abolishment_amount = 0
            
            if options['show_thw_fwd']:
                total_column += [{'name': th_amount, 'class': 'number'}]
            if options['show_creation']:
                total_column += [{'name': creation_amount, 'class': 'number'}]
            if options['show_abolishment']:
                total_column += [{'name': abolishment_amount, 'class': 'number'}]


            if options['show_budget_amount']:
                total_column += [{'name': '{:,.2f}'.format(budget_amount), 'class': 'number'}]
            
            practical_amount = self.env['hr.contract'].search_count([('state','=','open'),('employee_id.project_id','=',project.get('id') )])
            
            if options['show_actual_amount']:
                total_column += [{'name': '{:,.2f}'.format(practical_amount), 'class': 'number'}]
            if options['show_variance_amount']:
                total_column += [{'name': '{:,.2f}'.format(budget_amount - practical_amount), 'class': 'number'}]
            if options.get('comparison') and options['comparison'].get('periods'):
                    for period in options['comparison']['periods']:
                        
                        total_query = """
                                SELECT
                                    SUM(hr_budget_line.budget_value) AS budget_value,
                                    SUM(hr_budget_line.throw_forward) AS throw_forward,
                                    SUM(hr_budget_line.creation) AS creation,
                                    SUM(hr_budget_line.abolishment) AS abolishment,
                                    hr_budget_line.project AS project        
                                FROM
                                    hr_budget_line
                                WHERE project = %s """
                        if options['date']:
                            total_query += " AND Date(hr_budget_line.bgt_date_from) >= %s "
                            total_query += " AND Date(hr_budget_line.bgt_date_to) <= %s "
                        total_query += " GROUP BY project"
                        self.env.cr.execute(total_query,(project.get('id'),period['date_from'],period['date_to']))    
                        re = self.env.cr.fetchone()
                        if re:
                            budget_amount = re[0] or 0.0
                            th_amount1 = re[1] or 0.0
                            creation_amount = re[2] or 0.0
                            abolishment_amount = re[3] or 0.0
                        else:
                            budget_amount = 0
                            th_amount1 = 0
                            creation_amount =0
                            abolishment_amount = 0
                        
                        if options['show_thw_fwd']:
                            total_column += [{'name': th_amount1, 'class': 'number'}]
                        if options['show_creation']:
                            total_column += [{'name': creation_amount, 'class': 'number'}]
                        if options['show_abolishment']:
                            total_column += [{'name': abolishment_amount, 'class': 'number'}]

                        if options['show_budget_amount']:
                            total_column += [{'name': '{:,.2f}'.format(budget_amount), 'class': 'number'}]
                        practical_amount = self.env['hr.contract'].search_count([('state','=','open'),('employee_id.project_id','=',project.get('id') )])
                        
                        if options['show_actual_amount']:
                            total_column += [{'name': '{:,.2f}'.format(practical_amount), 'class': 'number'}]
                        if options['show_variance_amount']:
                            total_column += [{'name': '{:,.2f}'.format(budget_amount - practical_amount), 'class': 'number'}]
            lines.append({
                    'id': 'total_line'+str(project.get('id')),
                    'name': 'Total',
                    'level': 2,
                    'unfoldable': False,
                    'class': 'total',
                    'columns': total_column,
                })

        return lines

    def get_report_name(self):
        return _('Comparison Report By Analytic Account')
    
    def get_templates(self):
        templates = super(ProjectBudget, self).get_templates()
        templates['search_template'] = 'custom_budget_hr.search_template_budget_var'
        return templates
