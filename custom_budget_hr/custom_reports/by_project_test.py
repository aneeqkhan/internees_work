# -*- coding: utf-8 -*-

import time
from datetime import datetime
from datetime import date,timedelta
from flectra import api, models, _, fields
from flectra.tools.misc import formatLang
from flectra.exceptions import UserError,ValidationError
import logging
from itertools import groupby
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT as DF,DEFAULT_SERVER_DATETIME_FORMAT as DTF
from dateutil import rrule
from dateutil import tz

class HrBudgetByProject(models.AbstractModel):
    _name = "hr.budget.project.report"
    _description = "Hr Budget By Project Report"
    _inherit = 'account.report'

    filter_date = {'date_from': '', 'date_to': '', 'filter': 'this_year' ,'filter': 'this_quarter','filter': 'this_month', 'filter': 'last_year','filter': 'last_quarter','filter': 'last_month'}

    
    filter_show_budget_value = False
    filter_show_actual_value = False
    filter_show_vacancies_value = False



    def get_columns_name(self, options):
        columns = []
        columns.append({'name': _('Project')})
        if options['show_budget_value']:
            columns.append({'name': _('Budget'),'class':'number'})
        if options['show_actual_value']:
            columns.append({'name': _('Held'),'class':'number'})
        if options['show_vacancies_value']:
            columns.append({'name': _('Vacant'),'class':'number'})
        
        return columns
    
    @api.model
    def get_lines(self, options, line_id=None):
        lines = []
        all_budgets = self.env['hr.budget'].search([('date_from','>=',options['date']['date_from']),('date_to','<=',options['date']['date_to'])],order='project_id asc')
        
        for project,budgets in groupby(all_budgets,lambda bgt: bgt.project_id.id):
            project_id = self.env['account.analytic.account'].browse(project)
            budget_ids = self.env['hr.budget'].browse([budget.id for budget in budgets])
            columns = []
            start_date = datetime.strptime(options['date']['date_from'],DF)
            end_date = datetime.strptime(options['date']['date_to'],DF)
            if options['show_budget_value']:    
                columns.append({'name': _(' '),'class':'number'})
            if options['show_actual_value']:
                columns.append({'name': _(' '),'class':'number'})
            if options['show_vacancies_value']:
                columns.append({'name': _(' '),'class':'number'})
            lines.append({
                'id': project_id.name + str(project_id.id),
                'name': project_id.name,
                'level': 2,
                'unfoldable': True,
                'unfolded': line_id == None and True or False,
                'columns': columns,
                })
            for budget in budget_ids:
                columns = []
                if options['show_budget_value']:    
                    columns.append({'name': _(' '),'class':'number'})
                if options['show_actual_value']:
                    columns.append({'name': _(' '),'class':'number'})
                if options['show_vacancies_value']:
                    columns.append({'name': _(' '),'class':'number'})
                lines.append({
                    'id': budget.name + str(budget.id),
                    'name': budget.name,
                    'level': 3,
                    'unfoldable': True,
                    'unfolded': line_id == None and True or False,
                    'parent_id':project_id.name + str(project_id), 
                    'columns': columns,
                    })
                for bgt_line in budget.line_ids:
                    line_columns = []
                    if options['show_budget_value']:    
                        line_columns.append({'name': _(bgt_line.budget_value),'class':'number'})
                    if options['show_actual_value']:
                        line_columns.append({'name': _(bgt_line.actual_value),'class':'number'})
                    if options['show_vacancies_value']:
                        line_columns.append({'name': _(bgt_line.vacancies_value),'class':'number'})
                    lines.append({
                        'id': bgt_line.name + str(bgt_line.id),
                        'name': bgt_line.name,
                        'level': 4,
                        'parent_id': budget.name + str(budget),
                        'unfoldable': False,
                        'columns': line_columns,
                        })
        return lines

    def get_report_name(self):
        return _('HR Budget Report')
    
    def get_templates(self):
        templates = super(HrBudgetByProject, self).get_templates()
        templates['search_template'] = 'custom_budget_hr.search_template_budget_by_project'
        return templates