# -*- coding: utf-8 -*-

import time
from datetime import datetime
from datetime import date,timedelta
from flectra import api, models, _, fields
from flectra.tools.misc import formatLang
from flectra.exceptions import UserError,ValidationError
import logging
from itertools import groupby
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT as DF,DEFAULT_SERVER_DATETIME_FORMAT as DTF
from dateutil import rrule
from dateutil import tz



class HrBudgetByProject(models.AbstractModel):
    _name = "hr.budget.project.report"
    _description = "Hr Budget By Analytic Account Report"
    _inherit = 'account.report'


    filter_date = {'date_from': '', 'date_to': '', 'filter': 'this_year' ,'filter': 'this_quarter','filter': 'this_month', 'filter': 'last_year','filter': 'last_quarter','filter': 'last_month'}
    filter_unfold_all = False

    filter_multiple1 = True          # for project
    filter_multiple1_ids = []

    filter_multiple2 = True         # for contract type
    filter_multiple2_ids = []
    
    filter_multiple3 = True          # for job position
    filter_multiple3_ids = []

    filter_multiple5 = True             # for branch
    filter_multiple5_ids = None
    
    
    filter_show_aa_type = False
    filter_show_job_position = False
    filter_show_department = False
    filter_show_budget_grade = False

    filter_show_contract_type = False

    filter_show_thw_fwd = False
    filter_show_creation = False
    filter_show_abolishment = False

    filter_show_budget_value = True
    filter_show_actual_value = True
    filter_show_vacancies_value = True

    filter_analytic_wise_sum = False
    filter_budget_wise_sum = False


    @api.multi
    def get_report_informations(self, options):
        options = self.get_options(options)
        options = self.apply_date_filter(options)
        searchview_dict = {'options': options, 'context': self.env.context}
        searchview_dict['multiple3_ids'] = [(t.id, t.name) for t in self.env['hr.job'].search([])] or False
        searchview_dict['multiple1_ids'] = [(t.id, t.name) for t in self.env['account.analytic.account'].search([])] or False
        searchview_dict['multiple2_ids'] = [(t.id, t.name) for t in self.env['hr.contract.type'].search([])] or False
        searchview_dict['multiple5_ids'] = [(t.id, t.name) for t in self.env['res.branch'].search([])] or False
        report_manager = self.get_report_manager(options)
        info = {'options': options,
                'context': self.env.context,
                'report_manager_id': report_manager.id,
                'buttons': self.get_reports_buttons(),
                'main_html': self.get_html(options),
                'searchview_html': self.env['ir.ui.view'].render_template(self.get_templates().get('search_template', 'custom_budget_hr.search_template_budget_by_project'), values=searchview_dict),
                }


        return info

    @api.model
    def get_options(self, vals):
        if self.filter_multiple1:
            self.filter_multiple1_ids = [] if self.filter_multiple1 else None
        if self.filter_multiple2:
            self.filter_multiple2_ids = [] if self.filter_multiple2 else None
        if self.filter_multiple3:
            self.filter_multiple3_ids = [] if self.filter_multiple3 else None
        if self.filter_multiple5:
            self.filter_multiple5_ids = [] if self.filter_multiple5 else None    
        res = super(HrBudgetByProject, self).get_options(vals)
        return res

    def get_columns_name(self, options):
        columns = []

        columns.append({'name': _('Analytic Account->Budget->Job Position')})

        if options['show_aa_type']:
            columns.append({'name': _('AA Type'),'class':'number'})

        if options['show_job_position']:
            columns.append({'name': _('Description'),'class':'number'})

        if options['show_contract_type']:
            columns.append({'name': _('Contract type'),'class':'number'})

        if options['show_department']:
            columns.append({'name': _('Department'),'class':'number'})
        if options['show_budget_grade']:
            columns.append({'name': _('Grade'),'class':'number'})

        if options['show_thw_fwd']:
            columns.append({'name': _('Throw Forward'),'class':'number'})
        if options['show_creation']:
            columns.append({'name': _('Creation'),'class':'number'})
        if options['show_abolishment']:
            columns.append({'name': _('Abolishment'),'class':'number'})

        if options['show_budget_value']:
            columns.append({'name': _('Budget'),'class':'number'})
        if options['show_actual_value']:
            columns.append({'name': _('Held'),'class':'number'})
        if options['show_vacancies_value']:
            columns.append({'name': _('Vacant'),'class':'number'})
        
        return columns
    
    @api.model
    def get_lines(self, options, line_id=None):
        lines = []
           
        if options.get('date'):    
            d_from = options['date']['date_from']
            d_to = options['date']['date_to']
            
            if options['date']['filter'] == 'this_quarter':
                year_in = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                month_quarter = datetime.strftime(datetime.now(),"%Y-%m-%d")[5:7]
                if int(month_quarter) > 0 and int(month_quarter) <= 3:
                    d_from = year_in + '-00' + '-00'
                    d_to = year_in + '-03' + '-31'
                if int(month_quarter) > 3 and int(month_quarter) <= 6:
                    d_from = year_in + '-04' + '-00'
                    d_to = year_in + '-06' + '-31'
                if int(month_quarter) > 6 and int(month_quarter) <= 9:
                    d_from = year_in + '-07' + '-00'
                    d_to = year_in + '-09' + '-31'
                if int(month_quarter) > 12 and int(month_quarter) <= 12:
                    d_from = year_in + '-10' + '-00'
                    d_to = year_in + '-12' + '-31'
            if options['date']['filter'] == 'this_month':
                year_in = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                d_from = datetime.strftime(datetime.now(),"%Y-%m-%d")[5:7]
                d_to = datetime.strftime(datetime.now(),"%Y-%m-%d")[5:7]
                d_from = year_in + '-' + d_from +'-00'
                d_to = year_in + '-' + d_to +'-31'
            if options['date']['filter'] == 'last_year':
                # d_from = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                # d_to = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                # d_from = str(int(d_from)-1)
                # d_to = str(int(d_to)-1)
                # d_from += '-00-00'
                # d_to += '-12-31'
                d_from = options['date']['date_from']
                d_to = options['date']['date_to']
                
            if options['date']['filter'] == 'last_quarter':
                year_in = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                month_quarter = datetime.strftime(datetime.now(),"%Y-%m-%d")[5:7]
                if int(month_quarter) > 0 and int(month_quarter) <= 3:
                    d_from = year_in + '-10' + '-00'
                    d_to = year_in + '-12' + '-31'
                if int(month_quarter) > 3 and int(month_quarter) <= 6:
                    d_from = year_in + '-01' + '-00'
                    d_to = year_in + '-03' + '-31'
                if int(month_quarter) > 6 and int(month_quarter) <= 9:
                    d_from = year_in + '-04' + '-00'
                    d_to = year_in + '-06' + '-31'
                if int(month_quarter) > 9 and int(month_quarter) <= 12:
                    d_from = year_in + '-07' + '-00'
                    d_to = year_in + '-09' + '-31'
               
            if options['date']['filter'] == 'last_month':
                year_in = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                d_from = datetime.strftime(datetime.now(),"%Y-%m-%d")[5:7]
                d_to = datetime.strftime(datetime.now(),"%Y-%m-%d")[5:7]
                d_from = str(int(d_from)-1)
                d_to = str(int(d_to)-1)
                if int(d_from) < 11:
                    d_from = year_in + '-0' + d_from +'-00'
                    d_to = year_in + '-0'+ d_to +'-31'
                else:
                    d_from = year_in + '-' + d_from +'-00'
                    d_to = year_in + '-'+ d_to +'-31'
               
            if options['date']['filter'] == 'this_year':
                d_from = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                d_to = datetime.strftime(datetime.now(),"%Y-%m-%d")[0:4]
                d_from += '-00-00'
                d_to += '-12-31'
                # d_from = options['date']['date_from']
                # d_to = options['date']['date_to']
                
     
        data = self.env['hr.budget'].search([])
        projects = self.env['account.analytic.account'].search([])

        if options['multiple5_ids']:
            l = []
            for i in options['multiple5_ids']:
                l.append(int(i))
            projects = self.env['account.analytic.account'].search([('branch_id','in',l)])    
        
        if options['multiple1_ids']:
            l = []
            for i in options['multiple1_ids']:
                l.append(int(i))
            projects = projects.filtered(lambda r: r.id in l) 


        if options['multiple2_ids']:
            l = []
            budgets= []
            for i in options['multiple2_ids']:
                l.append(int(i))
            for d in data:
                for rec in d.line_ids:
                    if rec.contract_type_id.id in l:
                        budgets.append(d.id)
            data = data.filtered(lambda r: r.id in budgets)
        
        if options['multiple3_ids']:
            l = []
            budgets= []
            for i in options['multiple3_ids']:
                l.append(int(i))
            for d in data:
                for rec in d.line_ids:
                    if rec.job_id.id in l:
                        budgets.append(d.id)
            data = data.filtered(lambda r: r.id in budgets)

                               
                                   


        count = 0
        project_list = []

        for p in projects:
            if p.id in options.get('unfolded_lines') or line_id == None:
                if (p.id not in project_list and p.id == line_id) or (p.id not in project_list and line_id == None):
                    project_list.append(p.id)
                    
                    cols = []
                    thw_fwd = 0
                    creation = 0
                    abolishment = 0
                    budget = 0
                    held = 0
                    vacant  = 0
                    
                    if options['analytic_wise_sum']:
                        for x in data:
                            if p.id == x.project_id.id:
                                date = x.date_from[0:10]
                                if date < d_from or date > d_to:
                                    continue
                                for childs in x.line_ids:
                                    thw_fwd = thw_fwd + childs.throw_forward
                                    creation = creation + childs.creation
                                    abolishment = abolishment + childs.abolishment
                                    budget = budget + childs.budget_value
                                    held = held + childs.actual_value
                                    vacant = vacant + childs.vacancies_value
                                    

                    if options['show_aa_type']:
                        cols.append({'name': _(p.aa_type),'class':'number'})
                    if options['show_job_position']:
                        cols.append({'name': _(' '),'class':'number'})
                    if options['show_contract_type']:
                        cols.append({'name': _(' '),'class':'number'})


                    if options['show_department']:
                        cols.append({'name': _(' '),'class':'number'})
                    if options['show_budget_grade']:
                        cols.append({'name': _(' '),'class':'number'})
                    
                    if options['show_thw_fwd']:
                        cols.append({'name': _(thw_fwd),'class':'number'})
                    if options['show_creation']:
                        cols.append({'name': _(creation),'class':'number'})
                    if options['show_abolishment']:
                        cols.append({'name': _(abolishment),'class':'number'})


                    if options['show_budget_value']:
                        cols.append({'name': _(budget),'class':'number'})
                    if options['show_actual_value']:
                        cols.append({'name': _(held),'class':'number'})
                    if options['show_vacancies_value']:
                        cols.append({'name': _(vacant),'class':'number'})

                    lines.append({
                        'id': p.id,
                        'name' : p.name,
                        'columns': cols,
                        'level': 2,
                        'unfoldable': True,
                        'unfolded': p.id in options.get('unfolded_lines'),
                    })
                
                # Second Line
                if p.id in options.get('unfolded_lines'):
                    for sub1 in data:
                        if sub1.project_id.id == p.id:
                            date = sub1.date_from[0:10]
                            if date < d_from or date > d_to:
                                continue
                            if line_id == sub1.id+p.id +123 or line_id == p.id or line_id == None or options.get('unfold_all'):
                                cols = []
                                
                               
                                thw_fwd = 0
                                creation = 0
                                abolishment = 0
                                budget = 0
                                held = 0
                                vacant  = 0
                                
                                if options['budget_wise_sum']:
                                    for childs in sub1.line_ids:
                                        thw_fwd = thw_fwd + childs.throw_forward
                                        creation = creation + childs.creation
                                        abolishment = abolishment + childs.abolishment
                                        budget = budget + childs.budget_value
                                        held = held + childs.actual_value
                                        vacant = vacant + childs.vacancies_value

                                if options['show_aa_type']:
                                    cols.append({'name': _(' '),'class':'number'})
                                if options['show_job_position']:
                                    cols.append({'name': _(' '),'class':'number'})

                                
                                if options['show_contract_type']:
                                    cols.append({'name': _(' '),'class':'number'})
    

                                if options['show_department']:
                                    cols.append({'name': _(' '),'class':'number'})
                                if options['show_budget_grade']:
                                    cols.append({'name': _(' '),'class':'number'})
                                
                                if options['show_thw_fwd']:
                                    cols.append({'name': _(thw_fwd),'class':'number'})
                                if options['show_creation']:
                                    cols.append({'name': _(creation),'class':'number'})
                                if options['show_abolishment']:
                                    cols.append({'name': _(abolishment),'class':'number'})

                                if options['show_budget_value']:
                                    cols.append({'name': _(budget),'class':'number'})
                                if options['show_actual_value']:
                                    cols.append({'name': _(held),'class':'number'})
                                if options['show_vacancies_value']:
                                    cols.append({'name': _(vacant),'class':'number'})
                                lines.append({
                                    'id': sub1.id+p.id +123 ,
                                    'parent_id' : p.id,
                                    'name' : sub1.name,
                                    'columns': cols,
                                    'level': 4,
                                    'unfoldable': True,
                                    'unfolded':sub1.id+p.id +123 in options.get('unfolded_lines') or options.get('unfold_all'),
                                })

                            if sub1.id+p.id +123 in options.get('unfolded_lines') or options.get('unfold_all') :
                                for cc in data:
                                    if sub1.project_id.id == p.id and cc.id == sub1.id:
                                        if line_id == sub1.id+p.id +123 or line_id == sub1.id+p.id+cc.id +321  or line_id == p.id or line_id == None or options.get('unfold_all'):
                                            
                                            date = cc.date_from[0:10]
                                            if date < d_from or date > d_to:
                                                continue
                                            
                                            for child_lines in cc.line_ids:
                                                cols = []
                                                if options['show_aa_type']:
                                                    cols.append({'name': _(' '),'class':'number'})
                                                if options['show_job_position']:
                                                    cols.append({'name': _( child_lines.name ),'class':'number'})
                                                
                                                if options['show_contract_type']:
                                                    cols.append({'name': _(child_lines.contract_type_id.name),'class':'number'})

                                                if options['show_department']:
                                                    cols.append({'name': _(child_lines.job_id.department_id.name),'class':'number'})
                                                if options['show_budget_grade']:
                                                    cols.append({'name': _(child_lines.job_id.grade),'class':'number'})
                                                
                                                if options['show_thw_fwd']:
                                                    cols.append({'name': _(child_lines.throw_forward ),'class':'number'})
                                                if options['show_creation']:
                                                    cols.append({'name': _(child_lines.creation ),'class':'number'})
                                                if options['show_abolishment']:
                                                    cols.append({'name': _(child_lines.abolishment ),'class':'number'})

                                                if options['show_budget_value']:
                                                    cols.append({'name': _(child_lines.budget_value),'class':'number'})
                                                if options['show_actual_value']:
                                                    cols.append({'name': _(child_lines.actual_value),'class':'number'})
                                                if options['show_vacancies_value']:
                                                    cols.append({'name': _(child_lines.vacancies_value),'class':'number'})
                                                lines.append({
                                                    'id': sub1.id+p.id+cc.id +321 ,
                                                    'parent_id' : sub1.id+p.id +123,
                                                    'name' : child_lines.job_id.name ,
                                                    'style':'margin-left:10px',
                                                    'columns': cols,
                                                    'level': 6,
                                                })
        # lines.append({
        #         'id': 'hr_budget_total',
        #         'name': _('Total'),
        #         'level': 0,
        #         'style': 'font-weight:bold',
        #         'columns': ''
        # })


        
                        
        return lines
    def get_report_name(self):
        return _('Hr Budget By Analytic Account Report')
    
    def get_templates(self):
        templates = super(HrBudgetByProject, self).get_templates()
        templates['search_template'] = 'custom_budget_hr.search_template_budget_by_project'
        return templates

    