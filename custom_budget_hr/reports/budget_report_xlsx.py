from datetime import datetime, date, timedelta
import base64
import io
import logging
# from typing import NoReturn
from odoo import models

class BudgetReportXlsx(models.AbstractModel):
    _name = 'report.custom_budget_hr.budget_report_xls'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, budget):

        def get_date_format(date):
            return date.strftime("%d/%m/%Y")

        user = self.env.user
        company = self.env.company 
        #Designing
        heading2 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#DFECEC', 	
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'black',
            "bg_color": '#F0F3F4',
        })
        main_data = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        left = workbook.add_format({
            "bold": 1,
            "align": 'left',
            "valign": 'vcenter',
            "font_size": '9',
        })
        worksheet = workbook.add_worksheet('Budget Report')
        worksheet.set_column('A:M',20)
        row = 1

        # Main heading of comapny data
        worksheet.merge_range('E1:H2', company.name, company_data)
        worksheet.merge_range('E3:H3', company.street ,company_data)
        worksheet.merge_range('E4:H4', company.email, company_data)
        worksheet.merge_range('E5:H5', company.phone ,company_data)
        
        #sub budget data
        worksheet.write('A8', 'No.', heading2)
        worksheet.write('B8', 'Budget Type', heading2)
        worksheet.write('C8', 'Analytic Account', heading2)

        #SUB headings of Periods
        worksheet.write('D8', 'Start From ', heading2)
        worksheet.write('E8', 'End To', heading2)

        #sub budget lines data
        worksheet.write('F8', 'Job Position', heading2)
        worksheet.write('G8', 'Department', heading2)
        worksheet.write('H8', 'Creation', heading2)
        worksheet.write('I8', 'Planned Amount', heading2)
        worksheet.write('J8', 'Held', heading2)
        worksheet.write('K8', 'Actual Amount', heading2)
        worksheet.write('L8', 'Vacant', heading2)
        worksheet.write('M8', 'Variance', heading2)
        
        #domain
        domain = []
        if data['form']['group_ids']:
            domain.append(('group_id', 'in', data['form']['group_ids']))
        if data['form']['project_ids']:
            domain.append(('project_id', 'in', data['form']['project_ids'])) 
        # logging.info(domain)
        budget_reports = self.env['hr.budget'].search(domain)

        row = 7
        s_no =  1
        for budget in budget_reports:
            row += 1
            worksheet.write(row, 0,s_no,main_data)
            worksheet.write(row, + 1,budget.group_id.name if budget.group_id.name else ' ',center)
            worksheet.write(row, + 2,budget.project_id.name if budget.project_id.name else ' ',center)
            worksheet.write(row, + 3, get_date_format(budget.date_from if budget.date_from else ' '),center)
            worksheet.write(row, + 4, get_date_format(budget.date_to if budget.date_to else ' '),center)
           
            #budget lines
            for budget_lines in budget.line_ids:
                worksheet.write(row,5, budget_lines.job_id.name if budget_lines.job_id.name else ' ', center)
                worksheet.write(row,6, budget_lines.department_id.name if budget_lines.department_id.name else ' ', center)
                worksheet.write(row,7, budget_lines.creation if budget_lines.creation else 0, center)
                worksheet.write(row,8, budget_lines.planned_amount  if budget_lines.planned_amount else 0 , center)
                worksheet.write(row,9, budget_lines.actual_value  if budget_lines.actual_value else 0 , center)
                worksheet.write(row,10, budget_lines.actual_amount if budget_lines.actual_amount else 0 , center)
                worksheet.write(row,11, budget_lines.vacancies_value  if budget_lines.vacancies_value else 0 , center)
                # logging.info(budget_lines.vacancies_value)
                # logging.info(budget_lines.variance_amount)
                worksheet.write(row,12, budget_lines.variance_amount if budget_lines.variance_amount else 0 , center)
                row += 1
        
            # row += 1
            s_no += 1

        #report printed by "user_name"
        worksheet.write(row + 2, 0, "Printed by: ", left)
        worksheet.write(row + 2, 1, user.name, center)