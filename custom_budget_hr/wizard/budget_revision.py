from odoo import api, fields, models
from odoo.exceptions import ValidationError

class HrBudgetRevisionApplication(models.TransientModel):
    
    _name="hr.budget.revision.application"
        
    name=fields.Float("Revised Value")
    # Revision From
    # from_project_id = fields.Many2one('account.analytic.account', string='Project',related='from_budget_id.project_id')
    from_project_id = fields.Many2one('account.analytic.account', string='Project')
    from_budget_id = fields.Many2one('hr.budget', string='Budget',domain=lambda self: self.from_project_id_change)
    # from_budget_id = fields.Many2one('project.budget', string='Budget',related='from_budget_line.budget_id')
    from_budget_line = fields.Many2one('hr.budget.line', string='Budget Line',domain=lambda self: self.from_budget_id_change)
    from_budget_line_budget_value = fields.Integer(string='Budget',related='from_budget_line.budget_value')
    from_budget_line_actual_value = fields.Integer(string='Actual',related='from_budget_line.actual_value')
    from_budget_line_vacancies_value = fields.Integer(string='Vacancies',related='from_budget_line.vacancies_value')
    
    #Revision To
    to_project_id = fields.Many2one('account.analytic.account', string='Project',related='to_budget_id.project_id')
    # to_project_id = fields.Many2one('account.analytic.account', string='Project')
    to_budget_id = fields.Many2one('hr.budget', string='Budget',related='to_budget_line.budget_id')
    # to_budget_id = fields.Many2one('project.budget', string='Budget',domain=lambda self: self.to_project_id_change)
    to_budget_line = fields.Many2one('hr.budget.line', string='Budget Line')
    # to_budget_line = fields.Many2one('budget.line', string='Budget Line',domain=lambda self: self.to_budget_id_change)
    to_budget_line_budget_value = fields.Integer(string='Budget',related='to_budget_line.budget_value')
    to_budget_line_actual_value = fields.Integer(string='Actaul',related='to_budget_line.actual_value')
    to_budget_line_vacancies_value = fields.Integer(string='Vacancies',related='to_budget_line.vacancies_value')
    revision_type = fields.Selection([
        ('head_to_head', 'From Budget Line'),
        ('direct','Direct Revision')
    ], string='Revision Type',default='direct',help='Direct Revision: Revised Value will replace Budget Value, From Budget Line: Revised Value will be deducted/added from/to selected budget line')

    budget_line = fields.Many2one('hr.budget.line', string='Budget Line',readonly=True) #used in direct revision
    revised_value = fields.Float(string='Revised Value')
    
    @api.onchange('from_project_id')
    def from_project_id_change(self):
        from_budget_ids = self.env['hr.budget'].search([('project_id','=',self.from_project_id.id)])
        if from_budget_ids:
           return {'domain': {'from_budget_id' : [('id', 'in', from_budget_ids.ids)]}}
        else:
            return {'domain': {'from_budget_id' : [('id', 'in', [])]}}
    
    @api.onchange('from_budget_id')
    def from_budget_id_change(self):
        from_budget_head_ids = self.env['hr.budget.line'].search([('budget_id','=',self.from_budget_id.id)])
        if from_budget_head_ids:
           return {'domain': {'from_budget_line' : [('id', 'in', from_budget_head_ids.ids)]}}
        else:
            return {'domain': {'from_budget_line' : [('id', 'in', [])]}}
    
    @api.onchange('to_project_id')
    def to_project_id_change(self):
        to_budget_ids = self.env['hr.budget'].search([('project_id','=',self.to_project_id.id)])
        if to_budget_ids:
           return {'domain': {'to_budget_id' : [('id', 'in', to_budget_ids.ids)]}}
        else:
            return {'domain': {'to_budget_id' : [('id', 'in', [])]}}
    
    @api.onchange('to_budget_id')
    def to_budget_id_change(self):
        to_budget_line_ids = self.env['hr.budget.line'].search([('budget_id','=',self.to_budget_id.id)])
        if to_budget_line_ids:
           return {'domain': {'to_budget_id' : [('id', 'in', to_budget_line_ids.ids)]}}
        else:
            return {'domain': {'to_budget_id' : [('id', 'in', [])]}}

    
    def revise_value(self):
        # raise ValidationError("Hi")
        if self.revision_type == 'head_to_head':
            if self.revised_value > self.from_budget_line_budget_value:
                raise ValidationError("Revised Amount cant be greater than Budget Line amount")
            #deducting revised amount from budget Line
            self.env['hr.budget.revisions'].create({
                'name':self.from_budget_line.budget_id.id,
                'analytic_account':self.from_budget_line.budget_id.project_id.id,
                'previous_budget_value': self.from_budget_line.budget_value,
                'after_revision_value': self.from_budget_line.budget_value-self.revised_value,
                'line_reference': self.from_budget_line.id,
                'job_id': self.from_budget_line.job_id.id})
            self.from_budget_line.write({
                'budget_value':self.from_budget_line.budget_value-self.revised_value})
            #adding revised amount to budget Line
            self.env['hr.budget.revisions'].create({
                'name':self.to_budget_line.budget_id.id,
                'analytic_account':self.to_budget_line.budget_id.project_id.id,
                'previous_budget_value':self.to_budget_line.budget_value, 
                'after_revision_value':self.to_budget_line.budget_value+self.revised_value,
                'line_reference':self.to_budget_line.id,
                'job_id':self.to_budget_line.job_id.id})
            self.to_budget_line.write({
                'budget_value':self.to_budget_line.budget_value+self.revised_value})
        else:
            BudgetLine = self.env['hr.budget.line'].search([('id', '=', self.budget_line.id)])
            # Creating Budget Revision History
            self.env['hr.budget.revisions'].create({'name':BudgetLine.budget_id.id,
                                                 'analytic_account':BudgetLine.budget_id.project_id.id,
                                                 'previous_budget_value':BudgetLine.budget_value, 
                                                 'after_revision_value':self.revised_value,
                                                 'line_reference':BudgetLine.id,
                                                 'job_id':BudgetLine.job_id.id})
            # Update the figure in the planned amount for budget 
            BudgetLine.write({'budget_value':self.revised_value})
            