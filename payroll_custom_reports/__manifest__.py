# -*- coding: utf-8 -*-

{
    'name': 'Xlsx Payroll',
    'category': 'Generic Modules/Human Resources',
    'version': '13.0.1',
    'summary': 'Manage your employee payroll records',
    # 'images': ['static/description/banner.gif'],
    'description': "Odoo 13 Payroll, Payroll, Odoo 13,Odoo Payroll, Odoo Community Payroll",
    'depends': [
        'hr_contract',
        'hr_holidays',
        'hr_payroll_community',
        'report_xlsx'
        #'hr_contract_types',
    ],
    'data': [
        
        

        # 'security/hr_xls_payroll_security.xml',
        'security/ir.model.access.csv',

        'wizard/xls_wizard.xml',

        'views/view.xml',
        
        

        # 'views/res_config_settings_views.xml',

       
        'report/xls_report.xml',
        
    ],
}
