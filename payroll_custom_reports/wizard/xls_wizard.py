import logging
from odoo import api, fields, models, _
from odoo.exceptions import UserError,Warning
from datetime import datetime, date, timedelta


class XlsReport(models.TransientModel):
    _name = 'wizard.xls.report'

    
    date_from=fields.Date(comodel_name='hr.payslip', string='Start Date', required=True)
    date_to=fields.Date(comodel_name='hr.payslip', string='End Date', required=True)
    
    def print_report_xls(self):

      
        data={
            'model': 'wizard.xls.report',
            'form': self.read()[0]

        #    'payslips': payslips,
        #    'from_data':self.read()[0]
        }

        return self.env.ref('payroll_custom_reports.action_report_xls_payslip').report_action(self, data=data)
