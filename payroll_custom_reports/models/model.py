from odoo import models, fields
from datetime import datetime, date, timedelta

class PayrollXlsx(models.Model):
    _name='xls.payroll'
    _inherit='hr.payslip'
   

    name=fields.Char('Name')
    end_date=fields.Date('End Date')
    start_date=fields.Date('Start Date')

