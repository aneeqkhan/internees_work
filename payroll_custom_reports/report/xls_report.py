from datetime import datetime, date, timedelta
from odoo import api, models, fields
from urllib.request import urlopen
import io
import os
import logging
from odoo import http
from odoo.http import content_disposition, request
import xlsxwriter
from time import strftime, gmtime


dirname = os.path.dirname(__file__)



class PayrollXlsx(models.AbstractModel):
    _name = 'report.payroll_custom_reports.xls_report'
    _inherit = 'report.report_xlsx.abstract'


    
   
    def generate_xlsx_report(self, workbook, data, payroll):
        
        # domain = [('date_from','>=', self.date_from), ('date_to','<=', self.date_to)]
        # if data.get('date_from'):
        #     domain.append(('date_from','>=', data.get('date_from')))
        # if data.get('date_to'):
        #     domain.append(('date_to','>=', data.get('date_to')))
        # user_date_format = self.env['res.lang']._lang_get(self.env.user.lang).date_format
        
        # date_from = datetime.strptime(str(self.date_from), '%Y-%m-d').strftime(str(user_date_format))
        # date_to = datetime.strptime(str(self.date_to), '%Y-%m-%d').strftime(str(user_date_format))
        
        # date_from = datetime.today()
        # date_to = datetime.today()
        
   

        # def get_date_format(date):
        #     date.strftime("%d/%m/%Y")

       

        main_heading1 = workbook.add_format({
            "bold": 1,
            "top": 2,
            "bottom": 2,
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'white',
            "bg_color": '#0280A0F',
            'font_size': '12',
            })
       

        center_b = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'white',
            "bg_color": '#0280A0F',
            "font_size": '10',
        })

        center_a = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#74B72E',
            "font_size": '10',
        })

        worksheet = workbook.add_worksheet('payslip reports')
        bold = workbook.add_format({'bold':True})
       
        payslips = self.env['hr.payslip'].search([('date_from', '>=',data['form']['date_from']),('date_to','<=',data['form']['date_to'])])
        
       
        worksheet.merge_range('A1:S7', 'Payroll Information', main_heading1)

       
        row = 10
        col = 0
        worksheet.write(row, col, 'S No',center_b)
        worksheet.write(row, col+1, 'Emp ID',center_b)
        worksheet.write(row, col+2, 'CNIC', center_b)
        worksheet.write(row, col+3, 'Name',center_b)
        worksheet.write(row, col+4, 'DOB', center_b)
        worksheet.write(row, col+5, 'Duty Location', center_b)
        worksheet.write(row, col+6, 'Designation', center_b)
        worksheet.write(row, col+7, 'Joining Date', center_b)
        worksheet.write(row, col+8, 'Salary Computaion', center_b)
        worksheet.write(row, col+9, 'Amount', center_b)
        row += 1
        s_no =  1
        for slip in payslips:
            
            print("slip",slip)
            row += 2
            worksheet.merge_range('A8:S8', slip.dept_id.name , center_a)
            worksheet.write(row, 0,s_no, center_b)
            worksheet.write(row, col+1, slip.employee_id.emp_id)    
            worksheet.write(row, col+2, slip.employee_id.national_id.nationality_name)
            worksheet.write(row, col+3, slip.employee_id.name)
            worksheet.write(row, col+4, str(slip.employee_id.birthday))
            worksheet.write(row, col+5, slip.employee_id.work_location_id.name) 
            worksheet.write(row, col+6, slip.job_id.name) 
            worksheet.write(row, col+7, str(slip.employee_id.joining_date))
        
        
            for salary in slip.line_ids:
                worksheet.write(row, col+7, salary.name) 
                worksheet.write(row, col+8, salary.amount)
              
                row += 1
            s_no += 1 