# -*- coding: utf-8 -*-
{
    'name': 'Recruitment Reports',
    'author' : 'Maria',
    'version': '1.1',
    'category': 'Human Resources',
    'summary': 'Maintaining records of candidates, applying for jobs',
    'description': "",
    'depends': [
        'base',
        'hr',
        'hr_employee_extension',
        'hr_recruitment_extension',
        'hr_contract',
        'hr_recruitment',
        'website_hr_recruitment',
        'mail',
        'calendar',
        'website',
        'survey',
        'res_branch',
        'contacts',
        "account",
        "product",
        "report_xlsx",
        
    ],
    'data': [
        # 'security/groups.xml',
        # 'security/ir.model.access.csv',
        'reports/report.xml',
        'reports/pdf_report.xml',
    
    ],   
}

