from datetime import datetime, date, timedelta
import base64
import io
from typing import NoReturn
from odoo import models

class BudgetReportXlsx(models.AbstractModel):
    _name = 'report.om_account_budget_reports.account_budget_report_xls'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, account_budget_report):
        company = self.env.company
        user = self.env.user

        # date format
        def get_date_format(date):
            return date.strftime("%d/%m/%Y")
            
        #Design
        heading2 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#E1E7E7',
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        seial_number = workbook.add_format({
            "border_color": "#ABABAB",
            "left": 1,
            "right": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'black',
            "bg_color": '#D1DADA',
        })
        worksheet = workbook.add_worksheet('Account Budget Report')
        worksheet.set_column('A:J',20)
     
        row = 1
        col = 0

        # Main heading of comapny data
        worksheet.merge_range('D1:F2', company.name, company_data)
        worksheet.merge_range('D3:F3', company.street ,company_data)
        worksheet.merge_range('D4:F4', company.email, company_data)
        worksheet.merge_range('D5:F5',company.phone ,company_data)

        #heading 
        worksheet.write('A9','No.',heading2)
        worksheet.write('B9', 'Budget Name', heading2)
        worksheet.write('C9', 'Responsible', heading2)
        worksheet.write('D9',' Start From',heading2)
        worksheet.write('E9','End To',heading2)
        worksheet.write('F9','Budgetary Position',heading2)
        worksheet.write('G9','Analytic Account',heading2)
        worksheet.write('H9','Planned Account',heading2)
        worksheet.write('I9','Practical Account',heading2)
        worksheet.write('J9','Variance',heading2)
    
        domain = []
        # if data['form']['user_ids']:
        #     domain.append(('user_id', 'in', data['form']['user_ids']))
       
        # if data['form']['branch_ids']:
        #     domain.append(('branch_id','in',data['form']['branch_ids']))
        account_budget_report = self.env['crossovered.budget'].search([])    

        row = 8
        s_no =  1
        for account_budget in account_budget_report:
            row += 1
            worksheet.write(row, 0,s_no,seial_number)
            worksheet.write(row,col + 1,account_budget.name if account_budget.name else ' ' ,center)
            worksheet.write(row,col + 2,account_budget.user_id.name if account_budget.user_id.name else ' ' ,center)
            worksheet.write(row,col + 3, get_date_format(account_budget.date_from if account_budget.date_from else ' ' ),center)
            worksheet.write(row,col + 4, get_date_format(account_budget.date_to if account_budget.date_to else ' ' ),center)
            
            for budget_line in account_budget.crossovered_budget_line:
                worksheet.write(row,5, budget_line.general_budget_id.name if budget_line.general_budget_id.name  else ' ', center)
                worksheet.write(row,6, budget_line.analytic_account_id.name if budget_line.analytic_account_id.name  else ' ', center)
                worksheet.write(row,7, budget_line.planned_amount if budget_line.planned_amount else 0 , center)
                worksheet.write(row,8, budget_line.practical_amount if budget_line.practical_amount  else 0 , center)
                worksheet.write(row,9, budget_line.budget_variance if budget_line.budget_variance   else 0 , center)
                row += 1

            # row += 2
            s_no += 1