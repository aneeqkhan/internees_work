{
    'name': 'Budget Reports Management ',
    'author': 'romayyah',
    'category': 'Accounting',
    'description': """Generate budget excel reports""",
    'summary': 'Account Budget Reports Management',
    'depends': ['account','om_account_budget','report_xlsx','custom_budget_hr'],
    'license': 'LGPL-3',
    'data': [
        'security/ir.model.access.csv',
        'views/account_budget_report.xml',
        'reports/budget_pdf.xml',
        'reports/report.xml',     
    ],
}