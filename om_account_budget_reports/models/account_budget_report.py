from odoo import models, fields , api , exceptions
import logging

class AccountBudgetReport(models.TransientModel):
   _name = 'account.budget.report'
   _description = 'Account Budget Report'

  
   # user_ids = fields.Many2many(comodel_name='res.users',string='Responsible' )
   branch_ids = fields.Many2many(comodel_name='res.branch' , string='Branch Id')
  
   
   def action_print_xlsx_report(self):
      data={
         'model':'account.budget.report',
         'form':self.read()[0],
      }
      return self.env.ref('om_account_budget_reports.account_budget_report_xlsx').report_action(self,data=data)