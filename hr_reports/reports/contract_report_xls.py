import base64
import io
from odoo import models

class ContractReportXlsx(models.AbstractModel):
    _name = 'report.hr_reports.contract_report_xls'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, contract):
        
        company = self.env.company 
        def get_date_format(date):
            return date.strftime("%d/%m/%Y")

        # Create Designs
        heading1 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#f6b26b', 
            'font_size': '10',
        })
        heading2 = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_color": 'black',
            "bg_color": '#aee595', 	
            'font_size': '10',
        })
        center = workbook.add_format({
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
            "num_format": "#,##0.00",
        })
        company_data = workbook.add_format({
            "bold": 1,
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '10',
            "font_color": 'black',
            "bg_color": '#aee595',
        })
        main_data = workbook.add_format({
            "border_color": "#ABABAB",
            "align": 'center',
            "valign": 'vcenter',
            "font_size": '9',
        })
        worksheet = workbook.add_worksheet('Contract Report')
        worksheet.set_column('A:I',15)
        worksheet.set_column('J:AA1',25)
        row = 1
        # col = 0

        # Main heading of comapny data
        worksheet.merge_range('F1:K2', company.name, company_data)
        worksheet.merge_range('F3:K3', company.street ,company_data)
        worksheet.merge_range('F4:K4', company.email, company_data)
        worksheet.merge_range('F5:K5',company.phone ,company_data)
        # headings of employee data
        worksheet.merge_range('A9:E9', 'Employee data', heading1)

        # sub heading of employee data
        worksheet.write('A10', 'Serial No.', heading2)
        worksheet.write('B10', 'Employee Id', heading2)
        worksheet.write('C10', 'Name', heading2)
        worksheet.write('D10', 'Department', heading2)
        worksheet.write('E10', 'Job Position', heading2)
     
        # headings contract details
        worksheet.merge_range('F9:J9', 'Contract Details', heading1)
        #sub Heading contract details
        worksheet.write('F10', 'Contract Type', heading2)
        worksheet.write('G10', 'Salary Structure', heading2)
        worksheet.write('H10', 'Start Date', heading2)
        worksheet.write('I10', 'End Date', heading2)
        worksheet.write('J10', 'Date of Joining', heading2)
       
        # #headings salary information
        worksheet.merge_range('K9:M9', 'Salary Information', heading1)
        #Sub heading salary information
        worksheet.write('K10', 'Salary Rule', heading2)
        worksheet.write('L10', 'Category', heading2)
        worksheet.write('M10', 'Amount', heading2)


        # # headings of payslip
        worksheet.merge_range('N9:S9', 'Payslip', heading1)
        # #Sub heading of payslip
        worksheet.write('N10', 'References', heading2)
        worksheet.write('O10', 'Employee', heading2)
        worksheet.write('P10', 'Payslip', heading2)
        worksheet.write('Q10', 'Date from', heading2)
        worksheet.write('R10', 'Date to', heading2)
        worksheet.write('S10', 'Status', heading2)

        # # headings of other info
        worksheet.merge_range('T9:AA9', 'Other Info', heading1)
        ##Sub heading of other info
        worksheet.write('T10', 'Total Increment', heading2)
        worksheet.write('U10', 'Total Income Tax', heading2)
        worksheet.write('V10', 'Total Tax-paid Months', heading2)
        worksheet.write('W10', 'Total Tax Rebate', heading2)
        worksheet.write('X10', 'Total Taxable Arrears', heading2)
        worksheet.write('Y10', 'Total Taxable Deductions', heading2)
        worksheet.write('Z10', 'Increment Deduction Amount', heading2)
        worksheet.write('AA10', 'Contract Status', heading2)

        #domain
        domain = []
        if data['form']['branch_ids']:
            domain.append(('branch_id', 'in', data['form']['branch_ids']))

        contract_reports = self.env['hr.contract'].search(domain)

        row = 9
        s_no =  1
        for contract in contract_reports:
            row += 1
            worksheet.write(row, 0,s_no,main_data)
            worksheet.write(row, + 1,contract.name,center)
            worksheet.write(row, + 2,contract.employee_id.name,center)
            worksheet.write(row, + 3,contract.department_id.name,center)
            worksheet.write(row, + 4,contract.job_id.name,center)

            #sub heading of Contract Terms
            worksheet.write(row, + 5,contract.type_id.name,center)
            worksheet.write(row, + 6,contract.struct_id.name,center)
            worksheet.write(row, + 7,get_date_format(contract.date_start),center)
            # if('date_end' == False):
            #     return 
            worksheet.write(row, + 8,str(contract.date_end),center)
            worksheet.write(row, + 9,str(contract.joining_date),center)

            row_q = row
            # r1 = row

            #salary info
            for salaryinfo in contract.sal_rule_ids:
                worksheet.write(row_q,10, salaryinfo.name, center)
                worksheet.write(row_q,11, salaryinfo.Category_id.name, center)
                worksheet.write(row_q,12, salaryinfo.amount.name, center)
                row_q += 1

            #payslip
            # for payslip in contract.payslips:
            #     worksheet.write(r1,13, payslip.number, center)
            #     worksheet.write(r1,14, payslip.name, center)
            #     worksheet.write(r1,15, payslip.employee_id, center)
            #     worksheet.write(r1,16, payslip.date_from, center)
            #     worksheet.write(r1,17, payslip.date_to, center)
            #     worksheet.write(r1,18, payslip.state, center)
            #     r1 += 1    

            # OTHER INFO TAB
            # worksheet.write(row, + 19,contract.   .name,center)
            # worksheet.write(row, + 20,contract.income_tax,center)
            # worksheet.write(row, + 21,contract.paid_income_tax_months.name,center)
            # worksheet.write(row, + 22,contract.tax_rebate,center)
            # worksheet.write(row, + 23,contract.   .name,center)
            # worksheet.write(row, + 24,contract.   .name,center)
            # worksheet.write(row, + 25,contract.   .name,center)
            # worksheet.write(row, + 26,contract.state,center)     
               
            row  = row_q
            row += 1
            s_no += 1