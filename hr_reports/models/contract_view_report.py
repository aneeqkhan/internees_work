from odoo import models, fields , api , exceptions
import logging

class ContractReport(models.TransientModel):
   _name = 'contract.report'
   _description = 'Employee Report'
  
   branch_ids = fields.Many2many(comodel_name='res.branch',string='Branches' )

   def action_print_xlsx_report(self):
      data={
         'model':'contract.report',
         'form':self.read()[0],
      
      }
      return self.env.ref('hr_reports.contract_report_xlsx').report_action(self,data=data)