{
'name': "Hr Contract Reports",
'summary': "Contract Excel Reports",
'description': """
Contract Reports
==============
Generate Employee Contract Excel Reports.
""",
'author': "romayyah",
# 'website': "http://www.example.com",
'category': '',
'version': '13.0.1',
'depends': [ 'base','hr','res_branch','report_xlsx','hr_contract','hr_employee_extension'],
'data': [
     'security/ir.model.access.csv',
     'views/contract_view_report.xml',
     'reports/report.xml'
    ],
}